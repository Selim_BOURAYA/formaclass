package fr.afpa.formaclass;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import fr.afpa.formaclass.dto.beans.EquipementDto;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.service.EquipementService;
import fr.afpa.formaclass.service.EtablissementService;

@SpringBootTest
public class FormaclassEquipementTests {

	@Autowired
	@Qualifier("equipementService")
	EquipementService equServ;
	@Autowired
	EtablissementService etabServ;
	
	@Test
	public void saveEquipement(){
		
		EtablissementDto etabDto = etabServ.getEtablissementById(1L);
		
		EquipementDto equDto = EquipementDto.builder()
											.nom("Ordinateur")
											.disponible(true)
											.quantiteTotale(12)
											.quantiteAllouee(4)
											.build();
		equServ.save(equDto);
	}
	
}