package fr.afpa.formaclass;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;
import fr.afpa.formaclass.dao.beans.EtablissementDao;
import fr.afpa.formaclass.dao.beans.SalleDao;
import fr.afpa.formaclass.dao.repository.EtablissementRepository;
import fr.afpa.formaclass.dao.repository.SalleRepository;
import fr.afpa.formaclass.dto.Mapper.SalleServiceMapper;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.dto.beans.SalleDto;
import fr.afpa.formaclass.service.SalleServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TestServicesSalleDataMock {
		
	@Mock
	private EtablissementRepository mockEtablissementRepository;
	@Mock
	private SalleRepository mockSalleRepository;
	@InjectMocks
	private SalleServiceImpl salleServiceImpl;
	@Mock
	private SalleServiceMapper salleMapper;
	@Mock
	private ModelMapper mp;
	
	@Before
	public void setUp() throws Exception {
		
		//on "virtualise" le repository salle avant le lancement des tests grace à spring data mock
		mockSalleRepository = RepositoryFactoryBuilder.builder().mock(SalleRepository.class);
	}

	@Test
	public void testSaveSpringDataMock() {
			
		//On instancie un etablissement et une salle DAO
		EtablissementDao etablissementDao = new EtablissementDao(1L, "nom", "voie", "ville", "email", "codePostal","telephone", true);
		SalleDao salleDao = new SalleDao(1L, "nom", 10, true, true, true, etablissementDao);
		
		//On sauvegarde les entitées dans les repositories virtuels
		mockEtablissementRepository.save(etablissementDao);
		mockSalleRepository.save(salleDao);
		
		//on vérifie que la salle enregistrée avec l'id 1 correspond bien à la salle que l'on avait instancié : test Ok
		assertEquals(mockSalleRepository.findById(1L).get(), salleDao);
	}
		
	@Test
	public void testSaveMockito() {
		
		//On instancie un etablissement et une salle Dto
		EtablissementDto etablissementDto = new EtablissementDto(2L, "nom", "voie", "ville", "email", "codePostal","telephone", true);
		SalleDto salleDto = new SalleDto(1L, "nom", 10, true, true, true, etablissementDto);

		//On instancie un etablissement et une salle DAO
		EtablissementDao etablissementDao = new EtablissementDao(2L, "nom", "voie", "ville", "email", "codePostal","telephone", true);
		SalleDao salleDao = new SalleDao(1L, "nom", 10, true, true, true, etablissementDao);
		
		// on renseigne notre "contexte" Mock afin d'indiquer ce que mockito doit renvoyer lors du lancement des méthode, ici l'objet salleDao
		Mockito.when(salleMapper.salleDtoToSalleDao(org.mockito.ArgumentMatchers.any())).thenReturn(salleDao);
				
		//On save un objet en appeleant la méthode salleMapper.salleDtoToSalleDao, ce qui executera la ligne précèdente)
		SalleDao salleMappe = salleMapper.salleDtoToSalleDao(salleDto);
				
		// on renseigne la suite de notre "contexte" Mock afin d'indiquer ce que mockito doit renvoyer lors du lancement des méthodes ci dessous, ici l'objet salleDto
		Mockito.when(salleServiceImpl.getSalleById(org.mockito.ArgumentMatchers.any(),org.mockito.ArgumentMatchers.any())).thenReturn(salleDto);
		when(salleMapper.salleDaoToSalleDto(org.mockito.ArgumentMatchers.any())).thenReturn(salleDto);
		
		// on appelle la méthode getSalleById(Long idSalle, EtablissementDao etablissement) de la couche service
		SalleDto salleById = salleServiceImpl.getSalleById(salleMapper.salleDaoToSalleDto(salleDao).getIdSalle(), etablissementDao.getIdEtablissement());
		
		//on vérifie que la salle enregistrée avec l'id etablissement des deux salles correspondent bien : test Ok
		assertTrue(salleMappe.getEtablissement().getIdEtablissement() == salleById.getEtablissement().getIdEtablissement());
	}
		
}
