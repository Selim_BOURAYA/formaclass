package fr.afpa.formaclass.security.jwt;

import java.util.Date;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import fr.afpa.formaclass.dto.beans.BasicUser;
import fr.afpa.formaclass.security.service.UserDetailsImpl;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;


/**
 * Classe utilitaire qui va nous permettre de générer un token. Egalement de récupérer des informations d'un token ( ici le mail ) et vérifier la validité du token
 * @author NEBULA
 *
 */

@Component
public class JwtUtils {

private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);
	
	@Value("${jwt.secret}")
	private String jwtSecret;
	@Value("${jwt.jwtExpirationInMs}")
	private int jwtExpiration;
	@Value("${jwt.expirationActivationCompte}")
	private int jwtExirationActivationCompte;
	@Value("${jwt.expirationForgotPassword}")
	private int jwtExirationForgotPassword;
	
	/**
	 *Va générer le token pour le principal authentifié grace au param Authentication (Représente le jeton pour une demande d'authentification) 
	 * @param authentication
	 * @return
	 */
	public String generateJwtToken(Authentication authentication) {
		
		Date now = new Date();
		Date exipiryDate =  new Date(now.getTime() + jwtExpiration);

		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
		String authorities = userPrincipal.getAuthorities()
										  .stream()
										  .map(GrantedAuthority::getAuthority)
										  .collect(Collectors.joining(","));
		

		
		return Jwts.builder()
				   .setSubject(userPrincipal.getEmail())
				   .setIssuedAt(new Date())
				   .claim("auth", authorities)
				   .setExpiration(exipiryDate)
				   .signWith(SignatureAlgorithm.HS512, jwtSecret)
				   .compact();
	}
	
	/**
	 * Genere un token pour l'activation d'un compte 
	 * @param user
	 * @return
	 */
	public String generateJwtTokenActivationCompte(BasicUser user) {
		
		Date now = new Date();
		Date exipiryDate = new Date(now.getTime() + jwtExirationForgotPassword);
		
		return Jwts.builder()
				   .setSubject(user.getEmail())
				   .setIssuedAt(new Date())
				   .setExpiration(exipiryDate)
				   .signWith(SignatureAlgorithm.HS512, jwtSecret)
				   .compact();
	}
	
	/**
	 * Genere un token pour un password oublié
	 * @param user
	 * @return
	 */
	public String generateJwtTokenForgetPassword(String mail) {
		
		Date now = new Date();
		Date exipiryDate = new Date(now.getTime() + jwtExirationActivationCompte);
		
		return Jwts.builder()
				   .setSubject(mail)
				   .setIssuedAt(new Date())
				   .setExpiration(exipiryDate)
				   .signWith(SignatureAlgorithm.HS512, jwtSecret)
				   .compact();
	}
	
	/**
	 * Récupère l'email du token
	 * @param authToken
	 * @return
	 */
	public String getEmailFromToken(String authToken) {

		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken).getBody().getSubject();
		
	}
	
	public boolean validateJwtToken(String authToken) {
		
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		}catch(SignatureException e) {
			logger.error("Invalid signature : {}", e.getMessage());
		}catch(MalformedJwtException e) {
			logger.error("Invalid JWT token : {}", e.getMessage());
		}catch(ExpiredJwtException e) {
			logger.error("Jwt token is exipred : {}", e.getMessage());
		}catch(UnsupportedJwtException e) {
			logger.error("Jwt token is unsupported : {}", e.getMessage());
		}catch(IllegalArgumentException e) {
			logger.error("Jwt claims string is empty : {}", e.getMessage());
		}
		
		return false;
	}
}
