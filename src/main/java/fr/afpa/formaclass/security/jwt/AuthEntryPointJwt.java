package fr.afpa.formaclass.security.jwt;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * 
 * @author NEBULA
 * Par défaut, BasicAuthenticationEntryPoint provisionné par Spring Security renvoie une page complète pour une réponse 401 Unauthorized au client. 
 * Cette représentation HTML de l’erreur est bien rendue dans un navigateur, mais elle ne convient pas à d’autres scénarios, tels qu’une API REST où une représentation json peut être préférée. 
 *C'est notre cas.
 */

@Component
public class AuthEntryPointJwt implements AuthenticationEntryPoint {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthEntryPointJwt.class);

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		
		logger.error("Erreur AuthEntryPointJwt : Unauthorized error{}", authException.getMessage());
		
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Erreur : Vos identifiants ne sont pas bon ou vous n'êtes pas autorisé a acceder a ce service.");

	}

}
