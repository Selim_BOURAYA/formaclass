package fr.afpa.formaclass.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import fr.afpa.formaclass.security.jwt.AuthEntryPointJwt;
import fr.afpa.formaclass.security.jwt.AuthTokenFilter;
import fr.afpa.formaclass.security.service.UserDetailsServiceImpl;


@Configuration
@EnableWebSecurity
// permet d'utiliser les annotation @PreAuthorize("hasRole('ADMIN') or hasRole('PROFESSEUR')")
@EnableGlobalMethodSecurity(prePostEnabled = true)

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	UserDetailsServiceImpl userDetailsService;
	
	@Autowired
	private AuthEntryPointJwt unauthorizedHandler;
	
	/**
	 * Indique dans la configuration que ma classe authenticationJwtTokenFilter est un Bean Spring issu de AuthTokenFilter.
	 * @return
	 */
	@Bean
	public AuthTokenFilter authenticationJwtTokenFilter() {
		return new AuthTokenFilter();
	}
	
	/**
	 * L'authenticationManager peut faire trois choses : retourner une Authentication ( authenticated = true) s'il peut vérifier que l'entree represente un userPrincipal valide, lancer un exception ou retourner un null 
	 */
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	
	/**
	 * Implementation de PasswordEncoder comme Bean Spring
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		System.err.println("Je suis dans la configuration");
		http.cors().and().csrf().disable()
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.authorizeRequests().antMatchers("/api/auth/**").permitAll()
			.antMatchers("/api/salles/**").permitAll()
			.antMatchers("/api/csv/**").permitAll()
			.antMatchers("/api/professeurs/**").permitAll()
			.antMatchers("/api/eleves/**").permitAll()
			// Ajouter ci dessous vos chemin a autoriser
			//.antMatchers("MON CHEMIN A INDIQUER").permitAll()
			//.antMatchers("/api/test/**").permitAll()
			.anyRequest().authenticated();
		
		http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		System.err.println("Je suis dans le configure 2");
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	

}
