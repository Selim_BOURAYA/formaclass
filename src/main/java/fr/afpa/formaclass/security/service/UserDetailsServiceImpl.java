package fr.afpa.formaclass.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dto.beans.AuthUserDto;
import fr.afpa.formaclass.service.AuthUserService;

/**
 * Cette  classe  impĺemente l’interface UserDetailsService. Elle implémente donc une methode verifiant l’existence d’un utilisateur selon la valeur de email et retournant un objet de l’interfaceUserDetails
 * @author NEBULA
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	AuthUserService authUserService;

	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		AuthUserDto user = authUserService.findByEmail(email)
										  .orElseThrow(() -> new UsernameNotFoundException("L'utilisateur n'a pas été trouvé avec ce mail :"+ email));
		
		return UserDetailsImpl.build(user);
	}

}
