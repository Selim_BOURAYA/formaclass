package fr.afpa.formaclass.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.formaclass.dto.beans.EleveDto;


public class CsvHelper {
	
  public static String TYPE = "application/vnd.ms-excel";
  public static String TYPE2 = "text/csv";
  static String[] HEADERs = { "CodePostal","Email","Nom","Note","Prenom","Telephone","Ville","Voie","IdEtablissement"};
  
  
  public static boolean hasCSVFormat(MultipartFile file) {

    if (TYPE.equals(file.getContentType()) || TYPE2.equals(file.getContentType())) {
      return true;
    }
    
    return false;
  }

  public static List<EleveDto> csvToEleves(InputStream is) {
    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        CSVParser csvParser = new CSVParser(fileReader,
            CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

      List<EleveDto> listEleves = new ArrayList<EleveDto>();

      Iterable<CSVRecord> csvRecords = csvParser.getRecords();

      for (CSVRecord csvRecord : csvRecords) {
        EleveDto eleve = new EleveDto(
              csvRecord.get("CodePostal"),
              csvRecord.get("Email"),
              csvRecord.get("Nom"),
             csvRecord.get("Note"),
             csvRecord.get("Prenom"),
             csvRecord.get("Telephone"),
             csvRecord.get("Ville"),
             csvRecord.get("Voie")        
            );

        listEleves.add(eleve);
      }

      return listEleves;
    } catch (IOException e) {
      throw new RuntimeException("erreur de lecture du fichier CSV eleves: " + e.getMessage());
    }
  }

}