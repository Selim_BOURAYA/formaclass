package fr.afpa.formaclass.dto.beans;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import fr.afpa.formaclass.controller.beans.Etablissement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CategorieDto {

	private Long idCategorie;
	
	private String type;
	
	private EtablissementDto etablissement;

	
}