package fr.afpa.formaclass.dto.beans;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ModeleDto {
	
	private Long idModele;
	
	private String titre;
	
	private int eleveMin;
	
	private int eleveMax;
	
	private String description;
	
	private CategorieDto categorie;
	
	private EtablissementDto etablissement;

}
