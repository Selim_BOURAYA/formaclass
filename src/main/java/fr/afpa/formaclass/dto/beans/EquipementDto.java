package fr.afpa.formaclass.dto.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EquipementDto {
	
	private Long idEquipement;
	private String nom;
	private boolean disponible;
	private int quantiteTotale;
	private int quantiteAllouee;
	
	private EtablissementDto etablissementDto;

}