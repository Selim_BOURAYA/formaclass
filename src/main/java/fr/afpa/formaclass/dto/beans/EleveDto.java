package fr.afpa.formaclass.dto.beans;

import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import fr.afpa.formaclass.dao.beans.EtablissementDao;
import fr.afpa.formaclass.dao.beans.SessionDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString

public class EleveDto {
	

	private Long idEleve;
	
	private String nom;
	
	private String prenom;
	
	private String voie;
	
	private String ville;
	
	private String codePostal;
	
	private String telephone;
	

	private String email;
	
	private boolean actif;
	
	private float note;
	
	private boolean enFormation;
	
	private boolean accepteFormation;
	

	private Set <ModeleDto> listeFormationSouhaitees;
	
	

	private Set <SessionDto> listeSessionEffectuees;
		

	private EtablissementDto etablissement;
	
	public EleveDto ( String codePostal, String email, String nom, String note, String prenom, String telephone, String ville, String voie) {
		
		this.codePostal = codePostal; 
		this.email = email;
		this.nom = nom;
		this.note = Float.parseFloat(note);
		this.prenom = prenom;
		this.telephone = telephone;
		this.ville = ville;
		this.voie = voie;
	}
}
