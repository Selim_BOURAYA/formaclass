package fr.afpa.formaclass.dto.beans;

import java.util.Date;

import fr.afpa.formaclass.dao.beans.CategorieDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ProfesseurDto {

	private Long idProfesseur;
	
	private String nom;
	
	private String prenom;
	
	private String voie;
	
	private String ville;
	
	private String codePostal;
	
	private String telephone;
	
	private boolean actif = true;
	
	private Date derniereActivite;
	
	private Date dateArchivage;
	
	private String specialite;
	
	private boolean enFormation;
	
	private CategorieDto categorie;
	
	private AuthUserDto authUser;
	
	private EtablissementDto etablissement;
	
}
