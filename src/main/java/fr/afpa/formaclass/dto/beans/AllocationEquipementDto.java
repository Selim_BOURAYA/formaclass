package fr.afpa.formaclass.dto.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AllocationEquipementDto {
	
	private Long idAllocation;
	private int quantite;
	
	private EquipementDto equipementDto;
	private SalleDto salleDto;

}