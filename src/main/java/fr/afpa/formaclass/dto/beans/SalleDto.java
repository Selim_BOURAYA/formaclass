package fr.afpa.formaclass.dto.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SalleDto {
	
	private Long idSalle;
	
	private String nom;
	
	private int capaciteMax;
	
	private boolean accessibilite;
	
	private boolean disponible;
	
	private boolean reservable;
	
	private EtablissementDto etablissement;


}
