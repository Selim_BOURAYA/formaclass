package fr.afpa.formaclass.dto.beans;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import fr.afpa.formaclass.dao.beans.RoleDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString

public class AuthUserDto {
	
	private Long idAuthUser;
	
	private String email;
	
	private String password;
	
	private boolean activeProfil;
	
	private String tokenForgetPassword;
	
	private String tokenActivationCompte;
	
	private Set<RoleDto> roles = new HashSet<>();

	public AuthUserDto(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	
	
}
