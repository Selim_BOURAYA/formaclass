package fr.afpa.formaclass.dto.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EtablissementDto {
	
	private Long idEtablissement;
	
	private String nom;
	
	private String voie;
	
	private String ville;
	
	private String email;
	
	private String codePostal;
	
	private String telephone;
	
	private boolean actif;

	
}
