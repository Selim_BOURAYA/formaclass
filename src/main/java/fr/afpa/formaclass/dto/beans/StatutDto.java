package fr.afpa.formaclass.dto.beans;


import fr.afpa.formaclass.service.beans.EStatut;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class StatutDto {
	

	private Long idStatut;
	
	private EStatut nomination;

}

