package fr.afpa.formaclass.dto.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter @NoArgsConstructor @AllArgsConstructor
@Builder
@ToString
public class BasicUser {

	private String nom;
	private String prenom;
	private String email;
	private String token;
	private String expirationTime;
	
}
