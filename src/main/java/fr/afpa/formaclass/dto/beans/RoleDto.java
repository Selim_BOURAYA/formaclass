package fr.afpa.formaclass.dto.beans;

import fr.afpa.formaclass.service.beans.ERole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString

public class RoleDto {
	
	private Long idRole;
	
	private ERole name;
	
	

}
