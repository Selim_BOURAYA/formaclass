package fr.afpa.formaclass.dto.beans;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString

public class SessionDto {
	
	
	private Long idSession;
	
	private String nomination;
	
	private LocalDate dateDebut;
	
	private LocalDate dateFin;
		
	
	private List <EleveDto> listeEleve;
		
	
	private ModeleDto modele;
	
	
	private StatutDto statut;
	
	
	private SalleDto salle;
	
	
	private ProfesseurDto professeur;
		

	private NotificationDto notification;
	

	private EtablissementDto etablissement;

}
