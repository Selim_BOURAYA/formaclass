package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.SalleDao;
import fr.afpa.formaclass.dto.beans.SalleDto;

@Service 
public class SalleServiceMapper {
	
	@Autowired
	ModelMapper mp;
	
	public SalleDao salleDtoToSalleDao(SalleDto salleDto) {
		
		return mp.map(salleDto, SalleDao.class);
	}
	
	public SalleDto salleDaoToSalleDto(SalleDao salleDao) {
		
		return mp.map(salleDao, SalleDto.class);
	}
	
	public List<SalleDao> salleDtoListToSalleDaoList(List <SalleDto> salleDtoList) {
		
		return salleDtoList.stream()
							.map(this::salleDtoToSalleDao)
							.collect(Collectors.toList());
	}
	
	public List<SalleDto> salleDaoListToSalleDtoList(List <SalleDao> salleDaoList) {
		
		return salleDaoList.stream()
				.map(this::salleDaoToSalleDto)
				.collect(Collectors.toList());
	}
}
