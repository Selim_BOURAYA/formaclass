package fr.afpa.formaclass.dto.Mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.AdminDao;
import fr.afpa.formaclass.dto.beans.AdminDto;


@Service
public class AdminServiceMapper {

	
	@Autowired
	ModelMapper mp;
	
	public AdminDao adminDtoToAdminDao(AdminDto adminDto) {
		
		return mp.map(adminDto, AdminDao.class); 
	}
	
	public AdminDto adminDaoToAdminDto(AdminDao adminDao) {
		
		return mp.map(adminDao, AdminDto.class); 
	}
	
}
