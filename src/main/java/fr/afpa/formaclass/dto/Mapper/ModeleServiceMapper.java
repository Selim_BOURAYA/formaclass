package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.CategorieDao;
import fr.afpa.formaclass.dao.beans.ModeleDao;
import fr.afpa.formaclass.dto.beans.CategorieDto;
import fr.afpa.formaclass.dto.beans.ModeleDto;

@Service
public class ModeleServiceMapper {
	
	@Autowired
	ModelMapper mp;
	
	public ModeleDto modDaoToModDto(ModeleDao modDao) {
		
		return mp.map(modDao, ModeleDto.class); 
	}
	
	public ModeleDao modDtoToModDao(ModeleDto modDto) {
		
		return mp.map(modDto, ModeleDao.class); 
	}
	
	
	
	public Optional<ModeleDto> optionalModDaoToOptionalModDto(Optional<ModeleDao> modDao) {
		if (modDao.isPresent()) {
			return Optional.of(mp.map(modDao.get(), ModeleDto.class)); 
		}
		return Optional.empty();
		
	}
	
	public List<ModeleDto> listModDaoToListModDto(List<ModeleDao> modDaoList) {

		return  modDaoList.stream()
				   		   .map(this::modDaoToModDto)
				           .collect(Collectors.toList());
	}

}
