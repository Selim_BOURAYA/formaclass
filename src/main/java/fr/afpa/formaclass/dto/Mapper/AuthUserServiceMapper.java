package fr.afpa.formaclass.dto.Mapper;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.AuthUserDao;
import fr.afpa.formaclass.dto.beans.AuthUserDto;

@Service
public class AuthUserServiceMapper {

	@Autowired
	ModelMapper mp;
	
	public Optional<AuthUserDto> AuthUserDaoToAuthUserDto(Optional<AuthUserDao> optionalAuthDao) {
		 return Optional.of(mp.map(optionalAuthDao.get(), AuthUserDto.class)); 
	}
	
	public AuthUserDao AuthUserDtoToAuthUserDao(AuthUserDto authDto) {
		
		return mp.map(authDto, AuthUserDao.class); 
	}
	
}
