package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.CategorieDao;
import fr.afpa.formaclass.dto.beans.CategorieDto;


@Service
public class CategoryServiceMapper {

	
	@Autowired
	ModelMapper mp;
	
	public CategorieDto catDaoToCatDto(CategorieDao catDao) {
		
		return mp.map(catDao, CategorieDto.class); 
	}
	
	public CategorieDao catDtoToCatDao(CategorieDto catDto) {
		
		return mp.map(catDto, CategorieDao.class); 
	}
	
	public Optional<CategorieDto> optionalCatDaoToOptionalCatDto(Optional<CategorieDao> catDao) {
		if (catDao.isPresent()) {
			return Optional.of(mp.map(catDao.get(), CategorieDto.class)); 
		}
		return Optional.empty();
		
	}
	
	public List<CategorieDto> listCatDaoToListCatDto(List<CategorieDao> catDaoList) {

		return  catDaoList.stream()
				   		   .map(this::catDaoToCatDto)
				           .collect(Collectors.toList());
	}
}
