package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.SessionDao;
import fr.afpa.formaclass.dto.beans.SessionDto;

@Service
public class SessionServiceMapper {
	
	@Autowired
	ModelMapper mp;
	
	public SessionDto sessDaoToSessDto(SessionDao sessDao) {
		
		return mp.map(sessDao, SessionDto.class); 
	}
	
	public SessionDao sessDtoToSessDao(SessionDto sessDto) {
		
		return mp.map(sessDto, SessionDao.class); 
	}
	
	
	
	public Optional<SessionDto> optionalSessDaoToOptionalSessDto(Optional<SessionDao> sessDao) {
		if (sessDao.isPresent()) {
			return Optional.of(mp.map(sessDao.get(), SessionDto.class)); 
		}
		return Optional.empty();
		
	}
	
	public List<SessionDto> listSessDaoToListSessDto(List<SessionDao> sessDaoList) {

		return  sessDaoList.stream()
				   		   .map(this::sessDaoToSessDto)
				           .collect(Collectors.toList());
	}

}
