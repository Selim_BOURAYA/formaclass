package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.afpa.formaclass.dao.beans.EleveDao;
import fr.afpa.formaclass.dto.beans.EleveDto;


@Service
public class EleveServiceMapper {

	
	@Autowired
	ModelMapper mp;
	
	public EleveDao eleveDtoToEleveDao(EleveDto eleveDto) {
		
		return mp.map(eleveDto, EleveDao.class); 
	}
	
	
	public EleveDto eleveDaoToEleveDto(EleveDao eleveDao) {
		
		return mp.map(eleveDao, EleveDto.class); 
	}
	
	
	public List<EleveDao> eleveDtoListToEleveDaoList(List <EleveDto> eleveDtoList) {
		
		return eleveDtoList.stream()
							.map(this::eleveDtoToEleveDao)
							.collect(Collectors.toList());
	}
	
	
	public List<EleveDto> eleveDaoListToEleveDtoList(List <EleveDao> eleveDaoList) {
		
		return eleveDaoList.stream()
				.map(this::eleveDaoToEleveDto)
				.collect(Collectors.toList());
	}
	
}
