package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.RoleDao;
import fr.afpa.formaclass.dto.beans.RoleDto;


@Service
public class RoleServiceMapper {
	

	@Autowired
	ModelMapper mp;
	
	public RoleDto roleDaoToRoleDto(RoleDao roleDao) {
		
		return mp.map(roleDao, RoleDto.class); 
	}
	
	public RoleDao roleDtoToRoleDao(RoleDto roleDto) {
		
		return mp.map(roleDto, RoleDao.class); 
	}
	
	public Optional<RoleDto> optionalRoleDaoToOptionalRoleDto(Optional<RoleDao> roleDao) {
		if (roleDao.isPresent()) {
			return Optional.of(mp.map(roleDao.get(), RoleDto.class)); 
		}
		return Optional.empty();
		
	}
	
	public List<RoleDto> RoleDaoToRoleDto(List<RoleDao> roleDaoList) {

		return  roleDaoList.stream()
				   		   .map(this::roleDaoToRoleDto)
				           .collect(Collectors.toList());
	}
	


}
