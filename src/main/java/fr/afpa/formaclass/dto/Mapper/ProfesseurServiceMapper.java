package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.afpa.formaclass.dao.beans.ProfesseurDao;
import fr.afpa.formaclass.dto.beans.ProfesseurDto;


@Service
public class ProfesseurServiceMapper {

	
	@Autowired
	ModelMapper mp;
	
	public ProfesseurDao professeurDtoToProfesseurDao(ProfesseurDto professeurDto) {
		
		return mp.map(professeurDto, ProfesseurDao.class); 
	}
	
	
	public ProfesseurDto professeurDaoToProfesseurDto(ProfesseurDao professeurDao) {
		
		return mp.map(professeurDao, ProfesseurDto.class); 
	}
	
	
	public List<ProfesseurDao> professeurDtoListToProfesseurDaoList(List <ProfesseurDto> professeurDtoList) {
		
		return professeurDtoList.stream()
							.map(this::professeurDtoToProfesseurDao)
							.collect(Collectors.toList());
	}
	
	
	public List<ProfesseurDto> professeurDaoListToProfesseurDtoList(List <ProfesseurDao> professeurDaoList) {
		
		return professeurDaoList.stream()
				.map(this::professeurDaoToProfesseurDto)
				.collect(Collectors.toList());
	}
	
}
