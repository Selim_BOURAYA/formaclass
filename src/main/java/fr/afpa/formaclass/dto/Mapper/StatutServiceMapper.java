package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.StatutDao;
import fr.afpa.formaclass.dto.beans.StatutDto;


@Service
public class StatutServiceMapper {
	

	@Autowired
	ModelMapper mp;
	
	public StatutDto statDaoToStatDto(StatutDao statDao) {
		
		return mp.map(statDao, StatutDto.class); 
	}
	
	public StatutDao statDtoToStatDao(StatutDto statDto) {
		
		return mp.map(statDto, StatutDao.class); 
	}
	
	public Optional<StatutDto> optionalStatDaoToOptionalStatDto(Optional<StatutDao> statDao) {
		if (statDao.isPresent()) {
			return Optional.of(mp.map(statDao.get(), StatutDto.class)); 
		}
		return Optional.empty();
		
	}
	
	public List<StatutDto> statDaoToStatDto(List<StatutDao> statDaoList) {

		return  statDaoList.stream()
				   		   .map(this::statDaoToStatDto)
				           .collect(Collectors.toList());
	}
	


}
