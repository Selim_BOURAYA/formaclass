package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.controller.beans.AllocationEquipementBean;
import fr.afpa.formaclass.dao.beans.AllocationEquipementDao;
import fr.afpa.formaclass.dto.beans.AllocationEquipementDto;

@Service
public class AllocationEquipementMapper {

	@Autowired
	ModelMapper mp;
	
	/**
	 * Methode pour mapper une allocation equipement Bean vers une allocation equipement DAO
	 * @param allocationEquipementBean
	 * @return allocEquDao
	 */
	public AllocationEquipementDao allocationEquipementBeanToDao(AllocationEquipementBean allocationEquipementBean) {
		AllocationEquipementDao allocEquDao = mp.map(allocationEquipementBean, AllocationEquipementDao.class);
		return allocEquDao;
	}
	
	/**
	 * Methode pour mapper une allocation equipement DTO vers une allocation equipement DAO
	 * @param allocEquDao
	 * @return allocEquDto
	 */
	public AllocationEquipementDto allocationEquipementDaoToDto(AllocationEquipementDao allocationEquipementDao) {
		AllocationEquipementDto allocEquDto = mp.map(allocationEquipementDao,  AllocationEquipementDto.class);
		return allocEquDto;
	}
	
	/**
	 * Methode pour mapper une allocation equipement DAO vers une allocation equipement DTO
	 * @param allocEqutDto
	 * @return allocEquDao
	 */
	public AllocationEquipementDao allocationEquipementDtoToDao(AllocationEquipementDto allocationEquipementDto) {
		AllocationEquipementDao allocEquDao = mp.map(allocationEquipementDto,  AllocationEquipementDao.class);
		return allocEquDao;
	}
	
	/**
	 * Methode pour mapper une liste d'allocations equipements DTO vers une liste d'allocations equipements DAO
	 * @param allocationequipementDtoList
	 * @return List allocEquDao
	 */
	public List<AllocationEquipementDao> allocationEquipementDtoListToAllocationEquipementDaoList(List <AllocationEquipementDto> allocationEquipementDtoList){
		return allocationEquipementDtoList.stream()
										  .map(this::allocationEquipementDtoToDao)
										  .collect(Collectors.toList());
	}
	
	/**
	 * Methode pour mapper une liste d'allocations equipements DAO vers une liste d'allocations equipements DTO
	 * @param allocationequipementDaoList
	 * @return List allocEquDto
	 */
	public List<AllocationEquipementDto> allocationEquipementDaoListToAllocationEquipementDtoList(List <AllocationEquipementDao> allocationEquipementDaoList){
		return allocationEquipementDaoList.stream()
										  .map(this::allocationEquipementDaoToDto)
										  .collect(Collectors.toList());
	}
	
}