package fr.afpa.formaclass.dto.Mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.controller.beans.EquipementBean;
import fr.afpa.formaclass.dao.beans.EquipementDao;
import fr.afpa.formaclass.dto.beans.EquipementDto;

@Service
public class EquipementServiceMapper {

	@Autowired
	ModelMapper mp;
	
	/**
	 * Methode pour mapper un equipement Bean vers un equipement DAO
	 * @param equipementBean
	 * @return equDao
	 */
	public EquipementDao equipementBeanToDao(EquipementBean equipementBean) {
		EquipementDao equDao = mp.map(equipementBean, EquipementDao.class);
		return equDao;
	}
	
	/**
	 * Methode pour mapper un equipement DAO vers un equipement DTO
	 * @param equDao
	 * @return equDto
	 */
	public EquipementDto equDaoToDto(EquipementDao equDao) {
		EquipementDto equDto = mp.map(equDao, EquipementDto.class);
		return equDto;
	}
	
	/**
	 * Methode pour mapper un equipement DTO vers un equipement DAO
	 * @param equDto
	 * @return equDao
	 */
	public EquipementDao equDtoToDao(EquipementDto equDto) {
		EquipementDao equDao = mp.map(equDto, EquipementDao.class);
		return equDao;
	}

	/**
	 * Methode pour mapper une liste d'equipements DAO vers une liste d'equipements DTO
	 * @param equipementDaoList
	 * @return List equDto
	 */
	public List<EquipementDto> equipementDaoListToEquipementDtoList(List <EquipementDao> equipementDaoList){
		return equipementDaoList.stream()
								.map(this::equDaoToDto)
								.collect(Collectors.toList());
	}
	
	/**
	 * Methode pour mapper une liste d'equipements DTO vers une liste d'equipements DAO
	 * @param equipementDtoList
	 * @return List equDao
	 */
	public List<EquipementDao> equipementDtoListToEquipementDaoList(List <EquipementDto> equipementDtoList){
		return equipementDtoList.stream()
								.map(this::equDtoToDao)
								.collect(Collectors.toList());
	}
}