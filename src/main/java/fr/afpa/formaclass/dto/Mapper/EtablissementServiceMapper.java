package fr.afpa.formaclass.dto.Mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.controller.beans.Etablissement;
import fr.afpa.formaclass.dao.beans.EtablissementDao;
import fr.afpa.formaclass.dto.beans.EtablissementDto;

@Service

public class EtablissementServiceMapper {

	@Autowired
	ModelMapper mp;
	
	public EtablissementDao etabToEtabDao(Etablissement etab) {
		
		return mp.map(etab, EtablissementDao.class); 
	}
	
	public EtablissementDao etabDtoToEtabDao(EtablissementDto etablissementDto) {
		
		return mp.map(etablissementDto, EtablissementDao.class); 
	}
	
	/**
	 * Méthode pour mapper un etablissement DAO vers un etablissement DTO
	 * @param etabDao
	 * @return mp.map(etabDao, EtablissementDto.class)
	 */
	public EtablissementDto etabDaoToEtabDto(EtablissementDao etabDao) {
		return mp.map(etabDao, EtablissementDto.class);
	}
	
}