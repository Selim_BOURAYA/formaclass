package fr.afpa.formaclass.service;

import java.util.Optional;

import fr.afpa.formaclass.dto.beans.AuthUserDto;

public interface AuthUserService {
	
	boolean existsByEmail(String email);
	
	Optional<AuthUserDto> findByEmail(String email);

	Long save(AuthUserDto user);

	boolean checkActivationToken(String email, String token);
	
	void activationCompte(String email);
	
	public boolean checkForgotPasswordToken( String token, String email);

	void resetForgetPasswordToken(String email, String password);
	
	boolean isAnAdmin(String email);
	
}
