package fr.afpa.formaclass.service;

import java.util.List;
import java.util.Optional;

import fr.afpa.formaclass.dto.beans.CategorieDto;

public interface CategoryService {
	
	
	 List<CategorieDto> save(CategorieDto cat, Long long1);
	List<CategorieDto> getAllCategory(Long idEtablissement);
	Optional<CategorieDto> getCategoryById(Long idCat, Long idEt);
	List<CategorieDto> updateCategory(CategorieDto cat, String xmnr);
	List<CategorieDto> deleteCategory(Long idCat, Long idEtab);

}
