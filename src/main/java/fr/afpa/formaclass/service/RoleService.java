package fr.afpa.formaclass.service;

import java.util.Optional;

import fr.afpa.formaclass.dto.beans.RoleDto;
import fr.afpa.formaclass.service.beans.ERole;

public interface RoleService {
	
	public Optional<RoleDto> findByName(ERole name);

}
