package fr.afpa.formaclass.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.AdminDao;
import fr.afpa.formaclass.dao.repository.AdminRepository;
import fr.afpa.formaclass.dto.Mapper.AdminServiceMapper;
import fr.afpa.formaclass.dto.beans.AdminDto;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminRepository adminRepo;
	@Autowired
	AdminServiceMapper adminMapper;
	
	@Override
	public Long save(AdminDto admin) {
		AdminDao adminDao = adminRepo.save(adminMapper.adminDtoToAdminDao(admin));
		return adminDao.getEtablissement().getIdEtablissement();
	}

}
