package fr.afpa.formaclass.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.EquipementDao;
import fr.afpa.formaclass.dao.repository.EquipementRepository;
import fr.afpa.formaclass.dto.Mapper.EquipementServiceMapper;
import fr.afpa.formaclass.dto.beans.EquipementDto;
import fr.afpa.formaclass.dto.beans.EtablissementDto;

@Service
@Qualifier("equipementService")
public class EquipementServiceImpl implements EquipementService {

	@Autowired
	private EquipementRepository equipRepo;
	@Autowired
	private EquipementServiceMapper equipMapper;
	private EquipementDao equipementDao;
	
	/**
	 * Redefinition de la methode save() de l'interface EquipementService
	 * Va sauvegarder un nouvel equipement
	 * @param EquipementDto
	 */
	@Override
	public Long save(EquipementDto equipementDto) {
		
		equipementDao = equipRepo.save(equipMapper.equDtoToDao(equipementDto));
		return equipementDao.getIdEquipement();
		
	}

	/**
	 * Redefinition de la methode getEquipementById() de l'interface EquipementService
	 * Permet de recuperer l'id de l'equipement voulu
	 * @param id
	 * @return equMap.equDaoToDto(equipementRepository.findById(id).get())
	 */
	@Override
	public EquipementDto getEquipementById(Long id, Long idEtablissement) {
		return equipMapper.equDaoToDto(equipRepo.findById(id).get());
	}
	
	/**
	 * Redefinition de la methode updateEquipement() de l'interface EquipementService
	 * Permet de mettre a jour les informations de l'equipement voulu
	 * @param EquipementDto
	 */
	@Override
	public void updateEquipement(EquipementDto equipementDto) {
		EquipementDao equDao = equipMapper.equDtoToDao(equipementDto);
		equipRepo.save(equDao);
	}

	/**
	 * Redefinition de la methode deleteEquipement() de l'interface EquipementService
	 * Permet de supprimer un equipement
	 * @param Long idEquipement
	 */
	@Override
	public Long deleteEquipement(Long idEquipement, Long idEtablissement) {
		
		if(equipRepo.findEquipementByIdEquipement(idEquipement, idEtablissement) != null) {
			equipRepo.delete(equipRepo.findById(idEquipement).get());
			return idEquipement;
			}
		return null;
	}

	/**
	 * Redefinition de la methode getAllMateriel() de l'interface EquipementService
	 * Permet de recuperer l'ensemble du materiel existant dans l'etablissement
	 * @param idEtablissement
	 */
	@Override
	public List<EquipementDto> getAllMateriel(Long idEtablissement) {
		return equipMapper.equipementDaoListToEquipementDtoList(equipRepo.findByIdEtablissement(idEtablissement));
	}

	/**
	 * Redefinition de la methode getMaterielDisponible() de l'interface EquipementService
	 * Permet de recuperer l'ensemble du materiel disponible dans l'etablissement
	 * @param idEtablissement
	 */
	@Override
	public List<EquipementDto> getMaterielDisponible(Long idEtablissement) {
		// TODO Auto-generated method stub
		return null;
	}

}