package fr.afpa.formaclass.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.controller.beans.CriteriaEleve;
import fr.afpa.formaclass.controller.beans.CriteriaProfesseur;
import fr.afpa.formaclass.controller.beans.CriteriaSalle;
import fr.afpa.formaclass.dao.beans.SalleDao;
import fr.afpa.formaclass.dao.repository.SalleRepository;
import fr.afpa.formaclass.dto.Mapper.SalleServiceMapper;
import fr.afpa.formaclass.dto.beans.EleveDto;
import fr.afpa.formaclass.dto.beans.ProfesseurDto;
import fr.afpa.formaclass.dto.beans.SalleDto;

@Service
public class SalleServiceImpl implements SalleService {

	@Autowired
	SalleRepository salleRepo;
	@Autowired
	SalleServiceMapper salleMapper;
	private SalleDao salleDao;
	private SalleDto salleDto;
	private List<SalleDto> listeSalle;

	private List<SalleDto> listeFiltree;

	/**
	 * @author Selim methode listant l'ensemble des salles rattachées à
	 *         l'etablissement, en fonction de l'id de ce dernier
	 */
	@Override
	public List<SalleDto> getAllSalle(Long idEtablissement) {

		return salleMapper.salleDaoListToSalleDtoList(salleRepo.findByIdEtablissement(idEtablissement));
	}

	/**
	 * @author Selim methode enregistrant une salle, préalablement renseignée avec
	 *         son attribut etablissement
	 */
	@Override
	public Long save(SalleDto salleDto) {

		salleDao = salleRepo.save(salleMapper.salleDtoToSalleDao(salleDto));
		return salleDao.getIdSalle();
	}

	/**
	 * @author Selim methode récuperant, grâce à son id et à l'id etablissement, une
	 *         salle depuis la base de donnée Cette salle est convertie en objet DTO
	 *         puis renvoyée
	 */
	@Override
	public SalleDto getSalleById(Long idSalle, Long idEtablissement) {

		salleDto = salleMapper.salleDaoToSalleDto(salleRepo.findByIdSalleAndEtablissement(idSalle, idEtablissement));

		if (salleDto != null) {
			return salleDto;
		}
		return null;
	}

	/**
	 * @author Selim methode de mise à jour d'une salle. La salleDto en entrée et
	 *         préalablement renseignée avec son attribut etablissement
	 */
	@Override
	public Long updateSalle(SalleDto salleDto) {

		salleDao = salleRepo.save(salleMapper.salleDtoToSalleDao(salleDto));
		return salleDao.getIdSalle();
	}

	/**
	 * @author Selim methode de suppression d'une salle, à partir de son id et à
	 *         l'id etablissement l'idSalle est renvoyée en cas de suppression
	 *         réussie
	 */
	@Override
	public Long removeSalle(Long idSalle, Long idEtablissement) {

		if (salleRepo.findByIdSalleAndEtablissement(idSalle, idEtablissement) != null) {
			salleRepo.delete(salleRepo.findById(idSalle).get());
			return idSalle;
		}
		return null;
	}

	/**
	 * @author Selim methode de recherche par critére d'une salle, à partir de la
	 *         liste des salles de l'etablissement (a ameliorer)
	 */
	@Override
	public List<SalleDto> getSalleByCriteria(CriteriaSalle criteria, Long idEtablissement) {

		System.out.println(criteria.toString());

		listeSalle = salleMapper.salleDaoListToSalleDtoList(salleRepo.findByIdEtablissement(idEtablissement));

		if (listeSalle != null) {

			listeFiltree = new ArrayList<>();

			for (SalleDto salle : listeSalle) {

				if (salle.getCapaciteMax() >= criteria.getCapaciteMax()	&& salle.isAccessibilite() == criteria.isAccessibilite() 
						&& salle.isReservable() == true && salle.isDisponible() == true) {
					listeFiltree.add(salle);
				}
			}
			return listeFiltree;
		}
		return null;
	}

	/**
	 * @author Selim methode de recherche du planning d'une salle, à partir de la
	 *         liste des salles de l'etablissement (dans le bon service ?)
	 */
	@Override
	public List<Object> getPlanningSalle(Long idSalle, Long idEtablissement) {

		salleDto = salleMapper.salleDaoToSalleDto(salleRepo.findByIdSalleAndEtablissement(idSalle, idEtablissement));
		// salleDto.getPlanning();
		return null;
	}

	//Concerne le Service Session
	@Override
	public List<SalleDto> findSalleByCriteria(CriteriaSalle criteria, Long IdEtablissement) {
		// TODO Auto-generated method stub
		return salleMapper.salleDaoListToSalleDtoList(salleRepo.findSalleByCriteria(criteria.getCapaciteMax(), criteria.isAccessibilite(), criteria.getDebutSession(), criteria.getFinSession(), IdEtablissement));
	}

}
