package fr.afpa.formaclass.service;

import java.util.List;

import fr.afpa.formaclass.dto.beans.AllocationEquipementDto;
import fr.afpa.formaclass.dto.beans.EquipementDto;

public interface AllocationEquipementService {

	Long save(AllocationEquipementDto allocEquipDto);
	AllocationEquipementDto getAllocationById(Long id);

	void updateAllocationEquipement (AllocationEquipementDto allocEquDto, Long idSalle);
	Long deleteAllocationEquipement (Long idAllocationEquipement, Long idSalle);
	
	List<AllocationEquipementDto> getAllocationEquipementBySalleId(Long idSalle);
	List<AllocationEquipementDto> getAllocationEquipementByEquipementId(Long idEquipement);
	
}