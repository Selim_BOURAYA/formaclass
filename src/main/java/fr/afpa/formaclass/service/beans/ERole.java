package fr.afpa.formaclass.service.beans;

public enum ERole {
	ROLE_ADMIN,
	ROLE_PROFESSEUR
}
