package fr.afpa.formaclass.service.beans;

public enum EStatut {
	A_VENIR,
	EN_COURS,
	TERMINE,
	ANNULE,
	INCOMPLET
}
