package fr.afpa.formaclass.service.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dto.beans.BasicUser;

/**
 * Classe utilitaire servant a l'envoi de mail
 * 
 * @author NEBULA
 *
 */
@Service
public class MailService {
	
	public final String ACTIVATION_COMPTE_LINK = "http://localhost:8080/formaclass/confirmation-inscription/";

	public final String FORGET_PASSWORD_LINK = "http://localhost:8080/formaclass/forget-password/";
	@Autowired
	private  JavaMailSender javaMailSender;

	/**
	 * Service permettant d'envoyer un message concernant l'activation du compte d'un user.
	 * @param user
	 */
	public  void sendActivationCompteMessage(BasicUser user) {
		
		String message = "Cher(e) "+user.getNom()+" "+user.getPrenom()
        +"\n\n Vous avez été inscrit sur le site de FormaClass."
         +"\n\n Vous devez maintenant cliquer sur ce lien afin de confirmer votre inscription."
         +"\n\n Ce lien exirera dans 24 heures."
        + "\n <a href='"+ACTIVATION_COMPTE_LINK+user.getToken()+"'>FormaClass.com</a>";

		System.err.println("Le basicUser dans le mail " +user);
		
		
		SimpleMailMessage msg = new SimpleMailMessage();
		
		msg.setSubject("Confirmation Inscription FormaClass");
		msg.setTo(user.getEmail());
		msg.setText(message);
	

		javaMailSender.send(msg);
		
	
	}

	public void sendForgetPasswordMessage(BasicUser user) {
		String message = 
        "\n\n Vous avez fait une demande de changement de mot de passe sur le site de FormaClass."
         +"\n\n Vous devez maintenant cliquer sur ce lien afin de pouvoir modifier votre mot de passe."
         +"\n\n Ce lien exirera dans 1 heure."
        + "\n <a href='"+FORGET_PASSWORD_LINK+user.getToken()+"'>FormaClass.com</a>";


		
		SimpleMailMessage msg = new SimpleMailMessage();
		
		msg.setSubject("Confirmation Inscription FormaClass");
		msg.setTo(user.getEmail());
		msg.setText(message);
	

		javaMailSender.send(msg);
		
	}
	


}
