package fr.afpa.formaclass.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.CategorieDao;
import fr.afpa.formaclass.dao.repository.CategoryRepository;
import fr.afpa.formaclass.dto.Mapper.CategoryServiceMapper;
import fr.afpa.formaclass.dto.beans.CategorieDto;

@Service
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	CategoryServiceMapper catMap;
	@Autowired
	CategoryRepository repoCat;
	

	@Override
	public List<CategorieDto> getAllCategory(Long idEtablissement) {
			return catMap.listCatDaoToListCatDto(repoCat.findAllByEtablissementAndNull(idEtablissement));
	}

	@Override
	public Optional<CategorieDto> getCategoryById(Long idCat, Long idEt) {
		Optional<CategorieDao> cat = repoCat.findByidCategorieAndIdEtablissement(idCat, idEt);
		if(cat.isPresent()) {
			return Optional.of(catMap.catDaoToCatDto(cat.get()));
		}else {
			return Optional.empty();
		}
			
	}

	@Override
	public List<CategorieDto> updateCategory(CategorieDto newCat, String idEtab) {
		System.out.println(newCat);
		if (repoCat.canDeleteOrUpdateCategory(newCat.getIdCategorie())) {
			Optional<CategorieDao> cat = repoCat.findByidCategorieAndIdEtablissement(newCat.getIdCategorie(), Long.valueOf(idEtab));
			cat.get().setType(newCat.getType());
			catMap.catDaoToCatDto(repoCat.save(cat.get()));
			return  getAllCategory(Long.valueOf(idEtab));
		}
		return null;

			
	}

	@Override
	public List<CategorieDto> deleteCategory(Long idCat, Long idEtab) {
		if(repoCat.canDeleteOrUpdateCategory(idCat)) {
			repoCat.deleteById(idCat);
			return getAllCategory(idEtab);
		}
		return null;
	}

	@Override
	public List<CategorieDto> save(CategorieDto cat, Long xmnr) {
		catMap.catDaoToCatDto(repoCat.save(catMap.catDtoToCatDao(cat)));
		return this.getAllCategory(xmnr);
	}

}
