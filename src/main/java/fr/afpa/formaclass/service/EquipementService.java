package fr.afpa.formaclass.service;

import java.util.List;

import fr.afpa.formaclass.dto.beans.EquipementDto;
import fr.afpa.formaclass.dto.beans.EtablissementDto;

/**
 *Interface utilisee pour la gestion des equipements
 */
public interface EquipementService {

	Long save(EquipementDto equipementDto);
	EquipementDto getEquipementById(Long id, Long idEtablissement);
	void updateEquipement(EquipementDto equipementDto);
	Long deleteEquipement(Long idEquipement, Long IdEtablissement);
	
	List<EquipementDto> getAllMateriel(Long idEtablissement);
	List<EquipementDto> getMaterielDisponible(Long idEtablissement);
	
}