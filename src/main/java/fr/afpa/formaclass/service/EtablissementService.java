package fr.afpa.formaclass.service;

import fr.afpa.formaclass.controller.beans.Etablissement;
import fr.afpa.formaclass.dto.beans.EtablissementDto;

public interface EtablissementService {
	
	boolean existsByName(String name);
	Long save(Etablissement etab);
	Long findIdEtablissementByAuthUserEmail(String email);
	
	EtablissementDto getEtablissementById(Long id);
	void updateEtablissement(EtablissementDto etabDto);
	
	
	void updateInActif(String email);
}
