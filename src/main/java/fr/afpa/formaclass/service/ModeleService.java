package fr.afpa.formaclass.service;


import java.util.List;
import java.util.Optional;


import fr.afpa.formaclass.dto.beans.ModeleDto;

public interface ModeleService {
	
	ModeleDto createModel(ModeleDto model, Long long1);
	ModeleDto updateModel(ModeleDto model);
	boolean deleteModel(Long idmodel, Long idEtab);
	List<ModeleDto> getAllModel(Long idEtab);
	Optional<ModeleDto> getModelById(Long idModel, Long idEtab);
	



}
