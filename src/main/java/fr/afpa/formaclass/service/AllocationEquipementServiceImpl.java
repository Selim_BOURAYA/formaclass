package fr.afpa.formaclass.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import fr.afpa.formaclass.dao.beans.AllocationEquipementDao;
import fr.afpa.formaclass.dao.repository.AllocationEquipementRepository;
import fr.afpa.formaclass.dto.Mapper.AllocationEquipementMapper;
import fr.afpa.formaclass.dto.beans.AllocationEquipementDto;
import fr.afpa.formaclass.dto.beans.EquipementDto;

public class AllocationEquipementServiceImpl implements AllocationEquipementService{

	@Autowired
	AllocationEquipementRepository allocEquRepo;
	@Autowired
	AllocationEquipementMapper allocEquMapper;
	
	/**
	 * Redefinition de la methode save() de l'interface AllocationEquipementService
	 * Va créer une nouvelle allocation équipement, mais non rattachée à une salle
	 * @param AllocationEquipementDto
	 */
	@Override
	public Long save(AllocationEquipementDto allocEquipDto) {
		AllocationEquipementDao allocationEquipementDao = allocEquRepo.save(allocEquMapper.allocationEquipementDtoToDao(allocEquipDto));
		return allocationEquipementDao.getIdAllocation();
	}
	
	/**
	 * Redefinition de la methode getAllocationById() de l'interface AllocationEquipementService
	 * Permet de recuperer l'id de l'allocation demandée
	 * @param id
	 */
	@Override
	public AllocationEquipementDto getAllocationById(Long id) {
		return allocEquMapper.allocationEquipementDaoToDto(allocEquRepo.findById(id).get());
	}

	/**
	 * Redefinition de la methode updateAllocationEquipement() de l'interface AllocationEquipementService
	 * @param id
	 */
	@Override
	public void updateAllocationEquipement(AllocationEquipementDto allocEquDto, Long idSalle) {
		AllocationEquipementDao allocEquDao = allocEquMapper.allocationEquipementDtoToDao(allocEquDto);
		allocEquRepo.save(allocEquDao);
	}

	/**
	 * Redefinition de la methode deleteAllocationEquipement() de l'interface AllocationEquipementService
	 * Permet de supprimer une allocation d'equipement
	 * @param Long idAllocationEquipement
	 */
	@Override
	public Long deleteAllocationEquipement(Long idAllocationEquipement, Long idSalle) {
		
		if(allocEquRepo.getAllocationEquipementBySalleId(idSalle) != null) {
			allocEquRepo.delete(allocEquRepo.findById(idSalle).get());
			return idAllocationEquipement;
		}
		return null;
	}

	
	/**
	 * Redefinition de la methode getAllocationEquipementBySalleId() de l'interface AllocationEquipementService
	 * Permet de recuperer une liste d'allocations materiel existantes dans la salle
	 * @param idSalle
	 */
	@Override
	public List<AllocationEquipementDto> getAllocationEquipementBySalleId(Long idSalle) {
		return allocEquMapper.allocationEquipementDaoListToAllocationEquipementDtoList(allocEquRepo.getAllocationEquipementBySalleId(idSalle));
	}

	/**
	 * Redefinition de la methode getAllocationEquipementByEquipementId() de l'interface AllocationEquipementService
	 * Permet de recuperer une liste d'allocations materiel existantes pour un materiel donné
	 * @param idSalle
	 */
	@Override
	public List<AllocationEquipementDto> getAllocationEquipementByEquipementId(Long idEquipement) {
		return allocEquMapper.allocationEquipementDaoListToAllocationEquipementDtoList(allocEquRepo.getAllocationEquipementByEquipementId(idEquipement));
	}
	
}