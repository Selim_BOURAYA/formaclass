package fr.afpa.formaclass.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.formaclass.dao.repository.EleveRepository;
import fr.afpa.formaclass.dto.Mapper.EleveServiceMapper;
import fr.afpa.formaclass.dto.beans.EleveDto;
import fr.afpa.formaclass.helper.CsvHelper;

@Service
public class CsvService {
	
	@Autowired
	private EleveRepository eleveRepository;
	@Autowired
	EleveServiceMapper eleveMapper;

	public void save(MultipartFile file) {
		try {
			List<EleveDto> listeEleves = CsvHelper.csvToEleves(file.getInputStream());
			eleveRepository.saveAll(eleveMapper.eleveDtoListToEleveDaoList(listeEleves));
		} catch (IOException e) {
			throw new RuntimeException("impossible d'enregistrer le fichier eleves.csv: " + e.getMessage());
		}
	}

}