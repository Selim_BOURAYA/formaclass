package fr.afpa.formaclass.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.repository.SessionRepository;
import fr.afpa.formaclass.dto.Mapper.SessionServiceMapper;
import fr.afpa.formaclass.dto.beans.SessionDto;

@Service
public class SessionServiceImpl implements SessionService {
	
	@Autowired
	SessionServiceMapper sessMap;
	@Autowired
	SessionRepository sessRepo;

	@Override
	public SessionDto create(SessionDto newS) {
		return sessMap.sessDaoToSessDto(sessRepo.save(sessMap.sessDtoToSessDao(newS)));
	}

	@Override
	public SessionDto update(SessionDto session) {
		return sessMap.sessDaoToSessDto(sessRepo.save(sessMap.sessDtoToSessDao(session)));
	}


	@Override
	public Optional<SessionDto> getSession(Long idSession) {
		return sessMap.optionalSessDaoToOptionalSessDto(sessRepo.findById(idSession));
	}

	@Override
	public List<SessionDto> getCompleteSession(Long IdEtablissement) {
		return sessMap.listSessDaoToListSessDto(sessRepo.findCompleteSession(IdEtablissement));
	}

	@Override
	public List<SessionDto> getIncompleteSession(Long IdEtablissement) {
		return sessMap.listSessDaoToListSessDto(sessRepo.findIncompleteSession(IdEtablissement));
	}

	@Override
	public List<SessionDto> getEndedOrCancelledSession(Long IdEtablissement) {
		return sessMap.listSessDaoToListSessDto(sessRepo.findEndedOrCancelledSession(IdEtablissement));
	}


	@Override
	public boolean checkIfSessionIsComplete(Long idSession) {
		return sessRepo.checkIfSessionIsComplete(idSession);
		
	}

	@Override
	public void sendMailConfirmSession(Long idSession) {
		// TODO Auto-generated method stub

	}

	@Override
	public SessionDto findActualSession(Long idProfesseur) {
		
		return sessMap.sessDaoToSessDto(sessRepo.findActualSessionByIdProfessor(idProfesseur));
	}

	@Override
	public List<SessionDto> findListSessionByIdProfesseur(Long IdProfesseur) {
		
		return sessMap.listSessDaoToListSessDto(sessRepo.findSessionByIdProfessor(IdProfesseur));
	}

	@Override
	public SessionDto findActualSessionEleve(Long idEleve) {
		
		return sessMap.sessDaoToSessDto(sessRepo.findActualSessionByIdEleve(idEleve));
	}

	@Override
	public List<SessionDto> findListSessionByIdEleve(Long idEleve) {
		
		return null;
		//return sessMap.listSessDaoToListSessDto(sessRepo.findSessionByEleve(idEleve));
	}
}
