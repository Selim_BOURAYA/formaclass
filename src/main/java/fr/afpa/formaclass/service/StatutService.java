package fr.afpa.formaclass.service;

import java.util.Optional;

import fr.afpa.formaclass.dto.beans.StatutDto;
import fr.afpa.formaclass.service.beans.EStatut;

public interface StatutService {
	
	public Optional<StatutDto> findByDenomination(EStatut nomination);


}
