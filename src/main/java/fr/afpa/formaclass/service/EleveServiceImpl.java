package fr.afpa.formaclass.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.controller.beans.CriteriaEleve;
import fr.afpa.formaclass.dao.beans.EleveDao;
import fr.afpa.formaclass.dao.repository.EleveRepository;
import fr.afpa.formaclass.dto.Mapper.EleveServiceMapper;
import fr.afpa.formaclass.dto.beans.EleveDto;

@Service
public class EleveServiceImpl implements EleveService {

	@Autowired
	EleveRepository eleveRepo;
	@Autowired
	EleveServiceMapper eleveMapper;
	private EleveDao eleveDao;
	private EleveDto eleveDto;

	/**
	 * @author Selim methode listant l'ensemble des eleves rattachées à
	 *         l'etablissement, en fonction de l'id de ce dernier
	 */
	@Override
	public List<EleveDto> getAllEleve(Long idEtablissement) {

		return eleveMapper.eleveDaoListToEleveDtoList(eleveRepo.findByIdEtablissement(idEtablissement));
	}

	/**
	 * @author Selim methode enregistrant une eleve, préalablement renseignée avec
	 *         son attribut etablissement
	 */
	@Override
	public Long save(EleveDto eleveDto) {

		eleveDao = eleveRepo.save(eleveMapper.eleveDtoToEleveDao(eleveDto));
		return eleveDao.getIdEleve();
	}

	/**
	 * @author Selim methode récuperant, grâce à son id et à l'id etablissement, une
	 *         eleve depuis la base de donnée Cette eleve est convertie en objet DTO
	 *         puis renvoyée
	 */
	@Override
	public EleveDto getEleveById(Long idEleve, Long idEtablissement) {

		eleveDto = eleveMapper.eleveDaoToEleveDto(eleveRepo.findByIdEleveAndEtablissement(idEleve, idEtablissement));

		if (eleveDto != null) {
			return eleveDto;
		}
		return null;
	}

	/**
	 * @author Selim methode de mise à jour d'une eleve. La eleveDto en entrée et
	 *         préalablement renseignée avec son attribut etablissement
	 */
	@Override
	public Long updateEleve(EleveDto eleveDto) {

		eleveDao = eleveRepo.save(eleveMapper.eleveDtoToEleveDao(eleveDto));
		return eleveDao.getIdEleve();
	}

	/**
	 * @author Selim methode de suppression d'une eleve, à partir de son id et à
	 *         l'id etablissement l'idEleve est renvoyée en cas de suppression
	 *         réussie
	 */
	@Override
	public Long removeEleve(Long idEleve, Long idEtablissement) {

		if (eleveRepo.findByIdEleveAndEtablissement(idEleve, idEtablissement) != null) {
			eleveRepo.delete(eleveRepo.findById(idEleve).get());
			return idEleve;
		}
		return null;
	}

	
	// Concerne le service Session
	
	/**
	 * Permet de récupérer les Eleves dont l'id est contenue dans la liste en parametre
	 */
	@Override
	public List<EleveDto> findEleveMatchWithIdList(List<Long> idEleve) {
		return eleveMapper.eleveDaoListToEleveDtoList(eleveRepo.findByIdEleveIn(idEleve));
	}

	/**
	 * Permet de récupérer les eleves parmis les critères indiqués dans l'objet criteria
	 */
	@Override
	public List<EleveDto> findElevesByCriteria(CriteriaEleve criteria, Long IdEtablissement) {
		
		return eleveMapper.eleveDaoListToEleveDtoList(eleveRepo.findEleveByCriteria(criteria.getIdModele(), /*criteria.getDateDebut(), criteria.getDateFin(),*/ IdEtablissement));
	}

}
