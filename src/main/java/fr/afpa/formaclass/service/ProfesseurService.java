package fr.afpa.formaclass.service;

import java.util.List;

import fr.afpa.formaclass.controller.beans.CriteriaProfesseur;
import fr.afpa.formaclass.dto.beans.ProfesseurDto;

public interface ProfesseurService {
	
	Long save(ProfesseurDto professeurDto);
	Long updateProfesseur(ProfesseurDto professeurDto);
	Long removeProfesseur(Long id, Long idEtablissement);
	ProfesseurDto getProfesseurById(Long idProfesseur, Long idEtablissement);
	List<ProfesseurDto> getAllProfesseur(Long idEtablissement);
	
	//Concerne le Service Session
	List<ProfesseurDto> findProfesseurByCriteria(CriteriaProfesseur criteria, Long IdEtablissement);
}
