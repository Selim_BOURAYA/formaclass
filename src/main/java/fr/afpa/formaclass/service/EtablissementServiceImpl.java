package fr.afpa.formaclass.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.controller.beans.Etablissement;
import fr.afpa.formaclass.dao.beans.EtablissementDao;
import fr.afpa.formaclass.dao.repository.EtablissementRepository;
import fr.afpa.formaclass.dto.Mapper.EtablissementServiceMapper;
import fr.afpa.formaclass.dto.beans.EtablissementDto;

@Service
public class EtablissementServiceImpl implements EtablissementService {
	
	@Autowired
	EtablissementRepository etabRepo;
	@Autowired
	EtablissementServiceMapper etabMapper;

	@Override
	public boolean existsByName(String name) {
		
		return etabRepo.existsEtablissementDaoByNom(name);
	}

	@Override
	public Long save(Etablissement etab) {
		EtablissementDao etabDao = etabRepo.save(etabMapper.etabToEtabDao(etab));
		return etabDao.getIdEtablissement();
	}

	/***
	 * Permet de retrouver l'etablissement relié a un AuthUser uniquement si son compte est activé
	 */
	@Override
	public Long findIdEtablissementByAuthUserEmail(String email) {
		return etabRepo.findIdEtablissementByAuthUserEmailAndActiveProfilTrue(email);
	}

	/**
	 * Redefinition de la methode getEtablissementById() de l'interface EtablissementService
	 * Permet de recuperer l'id de l'etablissement cree, necessaire dans les differents objets
	 * @param Long id
	 * @return etabMapper.etabDaoToEtabDto(etabRepo.findById(id).get())
	 */
	@Override
	public EtablissementDto getEtablissementById(Long id) {
		return etabMapper.etabDaoToEtabDto(etabRepo.findById(id).get());
	}

	/**
	 * Redefinition de la methode update() de l'interface EtablissementService
	 * Permet de mettre a jour les informations de l'etablissement
	 */
	@Override
	public void updateEtablissement(EtablissementDto etabDto) {
		EtablissementDao etabDao = etabMapper.etabDtoToEtabDao(etabDto);
		etabRepo.save(etabDao);
	}

	
	/**
	 * trouve l'établissement correspondant à l'email de l'admin ayant activé son compte et active l'établissement
	 */
	@Override
	public void updateInActif(String email) {
		EtablissementDao etab = etabRepo.findEtablissementByAuthUserMail(email);
		etab.setActif(true);
		etabRepo.save(etab);
		
		
	}
	
	

}
