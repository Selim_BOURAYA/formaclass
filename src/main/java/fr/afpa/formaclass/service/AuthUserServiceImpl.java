package fr.afpa.formaclass.service;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.beans.AuthUserDao;
import fr.afpa.formaclass.dao.beans.RoleDao;
import fr.afpa.formaclass.dao.repository.AuthUserRepository;
import fr.afpa.formaclass.dto.Mapper.AuthUserServiceMapper;
import fr.afpa.formaclass.dto.beans.AuthUserDto;

@Service
@Qualifier("authUserService")
public class AuthUserServiceImpl implements AuthUserService {
	
	@Autowired
	AuthUserRepository authUserRepo;
	@Autowired
	AuthUserServiceMapper authUMapper;

	@Override
	public boolean existsByEmail(String email) {
		return authUserRepo.existsByEmail(email);
	}

	@Override
	public Optional<AuthUserDto> findByEmail(String email) {
		return authUMapper.AuthUserDaoToAuthUserDto(authUserRepo.findByEmail(email));
	}

	@Override
	public Long save(AuthUserDto authUserDto) {
		AuthUserDao AuthUserDao = authUserRepo.save(authUMapper.AuthUserDtoToAuthUserDao(authUserDto));
		return AuthUserDao.getIdAuthUser();
	}

	/**
	 * Verifie si le token indiqué en parametre existe et correspond au bon user.
	 */
	@Override
	public boolean checkActivationToken(String email, String token) {
		if(!authUserRepo.existsByEmailAndTokenActivationCompte(email, token)) {
			return false;
		}
		
		this.activationCompte(email);
		return true;
	}

	/**
	 * Verifie si le token indiqué en parametre existe et correspond au bon user.
	 */
	@Override
	public boolean checkForgotPasswordToken( String token, String email) {
		if(!authUserRepo.existsByTokenForgetPasswordAndEmail(token, email)) {
			return false;
		}
	
		return true;
	}
	
	
	@Override
	public void activationCompte(String email) {
		Optional<AuthUserDao> authDao = authUserRepo.findByEmail(email);
		authDao.get().setTokenActivationCompte(null);
		authDao.get().setActiveProfil(true);
		authUserRepo.save(authDao.get());
	}
	
	@Override
	public void resetForgetPasswordToken(String email, String password) {
		Optional<AuthUserDao> authDao = authUserRepo.findByEmail(email);
		authDao.get().setTokenForgetPassword(null);
		authDao.get().setPassword(password);
		authUserRepo.save(authDao.get());
	}

	/**
	 * Vérifie si l'authUser correspondant a l'email est un admin
	 */
	@Override
	public boolean isAnAdmin(String email) {
		Optional<AuthUserDao> authDao = authUserRepo.findByEmail(email);
		
		return authDao.get().getRoles().stream()
									   .allMatch(role -> role.getName().equals("ROLE_ADMIN"));
		
		
	}
	
	

}
