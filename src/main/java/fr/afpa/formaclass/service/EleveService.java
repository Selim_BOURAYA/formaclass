package fr.afpa.formaclass.service;

import java.util.List;

import fr.afpa.formaclass.controller.beans.CriteriaEleve;
import fr.afpa.formaclass.dto.beans.EleveDto;

public interface EleveService {
	
	Long save(EleveDto professeurDto);
	Long updateEleve(EleveDto professeurDto);
	Long removeEleve(Long id, Long idEtablissement);
	EleveDto getEleveById(Long idEleve, Long idEtablissement);
	List<EleveDto> getAllEleve(Long idEtablissement);
	
	//Service concernant les sessions
	List<EleveDto> findEleveMatchWithIdList(List<Long> idEleve);
	List<EleveDto> findElevesByCriteria(CriteriaEleve criteria, Long IdEtablissement);
}
