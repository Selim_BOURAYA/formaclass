package fr.afpa.formaclass.service;

import java.util.List;

import fr.afpa.formaclass.controller.beans.CriteriaEleve;
import fr.afpa.formaclass.controller.beans.CriteriaSalle;
import fr.afpa.formaclass.dto.beans.EleveDto;
import fr.afpa.formaclass.dto.beans.SalleDto;

public interface SalleService {
	
	Long save(SalleDto salleDto);
	Long updateSalle(SalleDto salleDto);
	Long removeSalle(Long id, Long idEtablissement);
	SalleDto getSalleById(Long idSalle, Long idEtablissement);
	List<SalleDto> getAllSalle(Long idEtablissement);
	List <SalleDto> getSalleByCriteria (CriteriaSalle criteria, Long idEtablissement);
	List <Object> getPlanningSalle(Long idSalle, Long idEtablissement);
	//checkAndChangeStatutSalle();
	
	
	//Concerne le Service Session
	List<SalleDto>  findSalleByCriteria(CriteriaSalle criteria, Long IdEtablissement);

}
