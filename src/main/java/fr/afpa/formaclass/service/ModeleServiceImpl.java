package fr.afpa.formaclass.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.repository.ModeleRepository;
import fr.afpa.formaclass.dto.Mapper.ModeleServiceMapper;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.dto.beans.ModeleDto;


@Service
public class ModeleServiceImpl implements ModeleService {

	@Autowired
	ModeleServiceMapper modMap;
	@Autowired
	ModeleRepository modRepo;
	
	@Override
	public ModeleDto createModel(ModeleDto model, Long idtab) {
		return modMap.modDaoToModDto(modRepo.save(modMap.modDtoToModDao(model)));
	}

	@Override
	public ModeleDto updateModel(ModeleDto model) {
		return modMap.modDaoToModDto(modRepo.save(modMap.modDtoToModDao(model)));
	}

	@Override
	public boolean deleteModel(Long idmodel, Long idEtab) {
			
		if(modRepo.ifAnySessionExtendsOfThisModel(idmodel, idEtab)) {
			return false;
		}else {
			modRepo.deleteById(idmodel);
			return true;
		}
		
		
	}

	@Override
	public List<ModeleDto> getAllModel(Long idEtab) {

		return modMap.listModDaoToListModDto(modRepo.findAllByIdEtablissement( idEtab));
	}

	@Override
	public Optional<ModeleDto> getModelById(Long idModel, Long idEtab) {
		return modMap.optionalModDaoToOptionalModDto(modRepo.findByIdModeleAndIdEtablissement(idModel , idEtab));
	}

}
