package fr.afpa.formaclass.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.repository.StatutRepository;
import fr.afpa.formaclass.dto.Mapper.StatutServiceMapper;
import fr.afpa.formaclass.dto.beans.StatutDto;
import fr.afpa.formaclass.service.beans.EStatut;


@Service
public class StatutServiceImpl implements StatutService {

	@Autowired
	StatutServiceMapper statMap;
	@Autowired
	StatutRepository statRepo;
	
	@Override
	public Optional<StatutDto> findByDenomination(EStatut nomination) {
		return statMap.optionalStatDaoToOptionalStatDto(statRepo.findByNomination(nomination));
	}

}
