package fr.afpa.formaclass.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.dao.repository.RoleRepository;
import fr.afpa.formaclass.dto.Mapper.RoleServiceMapper;
import fr.afpa.formaclass.dto.beans.RoleDto;
import fr.afpa.formaclass.service.beans.ERole;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleRepository roleRepo;
	@Autowired
	RoleServiceMapper rMapper;
	
	@Override
	public Optional<RoleDto> findByName(ERole name) {
		return rMapper.optionalRoleDaoToOptionalRoleDto(roleRepo.findByName(name));
	}

}
