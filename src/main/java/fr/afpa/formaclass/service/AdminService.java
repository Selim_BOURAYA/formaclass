package fr.afpa.formaclass.service;

import fr.afpa.formaclass.dto.beans.AdminDto;

public interface AdminService {
	
	Long save(AdminDto admin);

}
