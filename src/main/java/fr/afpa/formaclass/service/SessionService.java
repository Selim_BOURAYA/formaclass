package fr.afpa.formaclass.service;

import java.util.List;
import java.util.Optional;

import fr.afpa.formaclass.dto.beans.SessionDto;

public interface SessionService {
	
	SessionDto create(SessionDto newSess);
	SessionDto update(SessionDto session);
	Optional<SessionDto> getSession(Long idSession);
	List<SessionDto> getCompleteSession(Long IdEtablissement);
	List<SessionDto> getIncompleteSession(Long IdEtablissement);
	List<SessionDto> getEndedOrCancelledSession(Long IdEtablissement);
	boolean checkIfSessionIsComplete(Long idSession);
	void sendMailConfirmSession(Long idSession);
	
	// méthodes de récupération de la session actuelle et de la liste des sessions d'un professeur
	SessionDto findActualSession(Long idProfesseur);
	List<SessionDto> findListSessionByIdProfesseur(Long idProfesseur);
	
	SessionDto findActualSessionEleve(Long IdEleve);
	List<SessionDto> findListSessionByIdEleve(Long IdEleve);
}
