package fr.afpa.formaclass.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.formaclass.controller.beans.CriteriaProfesseur;
import fr.afpa.formaclass.dao.beans.ProfesseurDao;
import fr.afpa.formaclass.dao.repository.ProfesseurRepository;
import fr.afpa.formaclass.dto.Mapper.ProfesseurServiceMapper;
import fr.afpa.formaclass.dto.beans.ProfesseurDto;

@Service
public class ProfesseurServiceImpl implements ProfesseurService {

	@Autowired
	ProfesseurRepository professeurRepo;
	@Autowired
	ProfesseurServiceMapper professeurMapper;
	private ProfesseurDao professeurDao;
	private ProfesseurDto professeurDto;

	/**
	 * @author Selim methode listant l'ensemble des professeurs rattachées à
	 *         l'etablissement, en fonction de l'id de ce dernier
	 */
	@Override
	public List<ProfesseurDto> getAllProfesseur(Long idEtablissement) {

		return professeurMapper.professeurDaoListToProfesseurDtoList(professeurRepo.findByIdEtablissement(idEtablissement));
	}

	/**
	 * @author Selim methode enregistrant une professeur, préalablement renseignée avec
	 *         son attribut etablissement
	 */
	@Override
	public Long save(ProfesseurDto professeurDto) {

		professeurDao = professeurRepo.save(professeurMapper.professeurDtoToProfesseurDao(professeurDto));
		return professeurDao.getIdProfesseur();
	}

	/**
	 * @author Selim methode récuperant, grâce à son id et à l'id etablissement, une
	 *         professeur depuis la base de donnée Cette professeur est convertie en objet DTO
	 *         puis renvoyée
	 */
	@Override
	public ProfesseurDto getProfesseurById(Long idProfesseur, Long idEtablissement) {

		professeurDto = professeurMapper.professeurDaoToProfesseurDto(professeurRepo.findByIdProfesseurAndEtablissement(idProfesseur, idEtablissement));

		if (professeurDto != null) {
			return professeurDto;
		}
		return null;
	}

	/**
	 * @author Selim methode de mise à jour d'une professeur. La professeurDto en entrée et
	 *         préalablement renseignée avec son attribut etablissement
	 */
	@Override
	public Long updateProfesseur(ProfesseurDto professeurDto) {

		professeurDao = professeurRepo.save(professeurMapper.professeurDtoToProfesseurDao(professeurDto));
		return professeurDao.getIdProfesseur();
	}

	/**
	 * @author Selim methode de suppression d'une professeur, à partir de son id et à
	 *         l'id etablissement l'idProfesseur est renvoyée en cas de suppression
	 *         réussie
	 */
	@Override
	public Long removeProfesseur(Long idProfesseur, Long idEtablissement) {

		if (professeurRepo.findByIdProfesseurAndEtablissement(idProfesseur, idEtablissement) != null) {
			professeurRepo.delete(professeurRepo.findById(idProfesseur).get());
			return idProfesseur;
		}
		return null;
	}

	//Concerne le Service Session
	@Override
	public List<ProfesseurDto> findProfesseurByCriteria(CriteriaProfesseur criteria, Long IdEtablissement) {
		return professeurMapper.professeurDaoListToProfesseurDtoList(professeurRepo.findProfesseursByCriteria(IdEtablissement, criteria.getIdCategorie(), criteria.getDateDebut(), criteria.getDateFin()));
		
	}
	
	


}
