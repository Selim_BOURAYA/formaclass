package fr.afpa.formaclass;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/* "Test branche Thomas" */

@Configuration
public class ApplicationConfig {

	 
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper;
	}
	
	
	
	
}
