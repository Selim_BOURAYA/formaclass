package fr.afpa.formaclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class FormaclassApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormaclassApplication.class, args);
	}

}
