package fr.afpa.formaclass.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.controller.beans.AdminInscription;
import fr.afpa.formaclass.controller.beans.Nom;
import fr.afpa.formaclass.controller.beans.request.EmailRequest;
import fr.afpa.formaclass.controller.beans.request.LoginRequest;
import fr.afpa.formaclass.controller.beans.request.PasswordRequest;
import fr.afpa.formaclass.controller.beans.response.JwtResponse;
import fr.afpa.formaclass.controller.beans.response.MessageResponse;
import fr.afpa.formaclass.dto.beans.AdminDto;
import fr.afpa.formaclass.dto.beans.AuthUserDto;
import fr.afpa.formaclass.dto.beans.BasicUser;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.dto.beans.RoleDto;
import fr.afpa.formaclass.security.jwt.JwtUtils;
import fr.afpa.formaclass.security.service.UserDetailsImpl;
import fr.afpa.formaclass.service.AdminService;
import fr.afpa.formaclass.service.AuthUserService;
import fr.afpa.formaclass.service.EtablissementService;
import fr.afpa.formaclass.service.RoleService;
import fr.afpa.formaclass.service.beans.ERole;
import fr.afpa.formaclass.service.utils.MailService;


/***
 * Controller concernant l'authentification. Tout ce qui concerne l'inscription et le login de l'user transitera authomatiquement par ce controller.
 * @author NEBULA
 *
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	AuthUserService authUserService;
	@Autowired
	RoleService roleService;
	@Autowired
	EtablissementService etabService;
	@Autowired
	AdminService adminService;
	@Autowired
	PasswordEncoder encoder;
	@Autowired
	MailService mailServ;
	@Autowired
	JwtUtils jwtUtils;
	
	
	@PostMapping("/inscription")
	public ResponseEntity<?> inscriptionAdmin( @RequestBody AdminInscription inscription){
		System.out.println("inscription" + inscription);
		Map<String, Boolean> error = new HashMap<>();
		if(etabService.existsByName(inscription.getEtablissement().getNom())) {
			error.put("etablissementIsExist", true);
		}
		if(authUserService.existsByEmail(inscription.getAuthUser().getEmail())) {
			error.put("userIsExist", true);	
		}
		if(!error.isEmpty()) {
			return ResponseEntity.ok()
					.body(error);
		}
		
		
		// récupération des informations et definition des roles de notre AuthUserDto
		AuthUserDto authUser = new AuthUserDto(inscription.getAuthUser().getEmail(), encoder.encode(inscription.getAuthUser().getPassword()));
		Set<String> strRoles = inscription.getAuthUser().getRole();	
		Set<RoleDto> roles = new HashSet<>();
		
		if(strRoles == null) {
			RoleDto userRole = roleService.findByName(ERole.ROLE_ADMIN)
										  .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		}
		authUser.setRoles(roles);
		
		
		// Implementation du basicUser qui va servir a définir le token et a être envoyé dans le service mail
		BasicUser basicUser = BasicUser.builder()
									   .email(inscription.getAuthUser().getEmail())
									   .nom(inscription.getNom())
									   .prenom(inscription.getPrenom())
									   .build();
		
		String token = jwtUtils.generateJwtTokenActivationCompte(basicUser);
		basicUser.setToken(token);
		
		authUser.setTokenActivationCompte(token);
		
		//Creation de l'établissement
		EtablissementDto etab = EtablissementDto.builder()
												.nom(inscription.getEtablissement().getNom())
												.telephone(inscription.getEtablissement().getTelephone())
												.voie(inscription.getEtablissement().getVoie())
												.ville(inscription.getEtablissement().getVille())
												.codePostal(inscription.getEtablissement().getCodePostal())
												.email(inscription.getEtablissement().getEmail())
												.build();
												
		AdminDto admin = AdminDto.builder()
								 .nom(inscription.getNom())
								 .prenom(inscription.getPrenom())
								 .telephone(inscription.getTelephone())
								 .voie(inscription.getVoie())
								 .ville(inscription.getVille())
								 .codePostal(inscription.getCodePostal())
								 .authUser(authUser)
								 .etablissement(etab)
								 .build();
		
		adminService.save(admin);
		
		mailServ.sendActivationCompteMessage(basicUser);
		
		return ResponseEntity.ok(new MessageResponse("User enregistré avec succés"));
		
	}
	
	@GetMapping("/confirmation-inscription/{token}")
	public ResponseEntity<?> confirmationInscription(@PathVariable String token){
		
		if(jwtUtils.validateJwtToken(token)) {
			String email = jwtUtils.getEmailFromToken(token);
			if(authUserService.checkActivationToken(email, token)) {
				if(authUserService.isAnAdmin(email)) {
					etabService.updateInActif(email);
				}
				return ResponseEntity.ok(new MessageResponse("Compte activé"));
			}
		}
		return ResponseEntity
				.badRequest()
				.body(MessageResponse.builder()
									 .message("Votre lien a expiré")
									 .build()
						);
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest){
		System.out.println(loginRequest);
		Authentication authentication = authenticationManager.authenticate( 
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword())
				);
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
														 .map(item -> item.getAuthority())
														 .collect(Collectors.toList());
		
		Long idEtablissement = etabService.findIdEtablissementByAuthUserEmail(loginRequest.getEmail());
		if(idEtablissement == null) {
			return ResponseEntity.badRequest().body("Merci d'activer votre compte avant de vous connecter");
		}
		 
		return ResponseEntity.ok(JwtResponse.builder()
											.token(jwt)
											.id(userDetails.getId())
											.email(userDetails.getEmail())
											.idEtablissement((idEtablissement))
											.roles(roles)
											.type("Bearer ")
											.build());
	}
	
	@PostMapping("/forget-password")
	public ResponseEntity<?> forgetPassword(@RequestBody EmailRequest email){
		System.out.println("le email"+ email);
		
		if(authUserService.existsByEmail(email.getEmail())) {

			Optional<AuthUserDto> authUser = authUserService.findByEmail(email.getEmail());
			String jwt = jwtUtils.generateJwtTokenForgetPassword(email.getEmail());
			authUser.get().setTokenForgetPassword(jwt);
			
			authUserService.save(authUser.get());
			System.out.println("le email AuthUser"+ authUser.get().getEmail());
			BasicUser basicUser = BasicUser.builder()
					   .email(authUser.get().getEmail())
					   .token(jwt)
					   .build();
			
			mailServ.sendForgetPasswordMessage(basicUser);
			return ResponseEntity.ok(MessageResponse.builder().message("Mail envoyé").build());
		}
		
		return ResponseEntity.badRequest()
							 .body(MessageResponse.builder().message("Le mail indiqué n'existe pas").build());
	}
	
	/**
	 * Verifie si le token forgotPassword en entrée est bien valide et qu'il existe en base de donnée. Envoi une badRequest si ce n'est pas le cas
	 * @param token
	 * @return
	 */
	
	@GetMapping("/forget-password/{token}")
	public ResponseEntity<?> forgetPassword(@PathVariable String token){
		
		System.out.println("token " + token);
		
		if(jwtUtils.validateJwtToken(token)) {
			String email = jwtUtils.getEmailFromToken(token);
			if(authUserService.checkForgotPasswordToken( token, email)) {
				HttpHeaders headers = new HttpHeaders();
			    headers.add("token", token);
				return new ResponseEntity<>(new MessageResponse("Token valide"), headers, HttpStatus.OK);
			}
		}
		
		return ResponseEntity
				.badRequest()
				.body(new MessageResponse("Votre lien a expiré"));		
	}
	
	
	@PostMapping("/forget-password/new-password")
	public ResponseEntity<?> newPassword(@RequestBody PasswordRequest passwordRequest, @RequestHeader String token){
		System.out.println("le token dans le header"+ token);

		if(jwtUtils.validateJwtToken(token)) {
			String email = jwtUtils.getEmailFromToken(token);
			if(authUserService.checkForgotPasswordToken( token, email)) {
				authUserService.resetForgetPasswordToken(email, encoder.encode(passwordRequest.getPassword()));
				return ResponseEntity.ok(MessageResponse.builder().message("Votre password a bien été modifié").build());
			}
		}
		
		return ResponseEntity.badRequest()
				 .body(MessageResponse.builder().message("Votre demande n'a pas abouti").build());
		
		
	}
	
	/**
	 * Permet de vérifier si le nom de l'établissement est déjà existant
	 * @param name
	 * @return
	 */
	@PostMapping("/check/etablissement")
	public Map<String, Boolean> checkEtablissementName(@RequestBody Nom nom){
		System.out.println("nom"+ nom.getNom());
		if(etabService.existsByName(nom.getNom())) {
			return Collections.singletonMap("etablissementIsExist", true);
		}
		
		return Collections.singletonMap("etablissementIsExist", false);
		
	}
	
	

	

}
