package fr.afpa.formaclass.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.controller.beans.CriteriaSalle;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.dto.beans.SalleDto;
import fr.afpa.formaclass.service.EtablissementServiceImpl;
import fr.afpa.formaclass.service.SalleServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/salles")
public class SalleController {

	@Autowired
	private SalleServiceImpl salleService;
	@Autowired
	private EtablissementServiceImpl etabService;
	private EtablissementDto etablissementDto;
	private List<SalleDto> listeSalle;
	private SalleDto salleDto;
	private Long idSalle;

	/**
	 * @author Selim methode de recuperation de la liste des salles
	 */
	@GetMapping("/liste-salles")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> affichageListe(@RequestHeader("xmnr") String idEtablissement) {

		listeSalle = salleService.getAllSalle(Long.parseLong(idEtablissement));

		if (listeSalle != null) {
			return ResponseEntity.ok(listeSalle);
		}
		return ResponseEntity.badRequest().body("aucune salle n'est enregistrée");
	}

	/**
	 * @author Selim methode d'ajout d'une salle depuis le formulaire dédié
	 *         (récupération du @RequestBody)
	 */
	@PostMapping("/ajouter-salle")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> postSalle(@RequestHeader("xmnr") String idEtablissement,
			@RequestBody SalleDto salleDto) {

		salleDto.setEtablissement(etabService.getEtablissementById(Long.parseLong(idEtablissement)));
		idSalle = salleService.save(salleDto);
		return ResponseEntity.ok("la salle " + idSalle + " a bien été enregistrée");
	}

	/**
	 * @author Selim methode de récupération d'une salle, à partir de son id
	 */
	@GetMapping("/salle/{idSalle}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('PROFESSEUR')")
	public ResponseEntity<?> getSalle(@RequestHeader("xmnr") String idEtablissement,
			@PathVariable Long idSalle) {

		salleDto = salleService.getSalleById(idSalle, Long.parseLong(idEtablissement));
		if (salleDto != null) {
			return ResponseEntity.ok(salleDto);
		}
		return ResponseEntity.ok("la salle demandée n'existe plus");
	}

	/**
	 * @author Selim methode de modification d'une salle, à partir de son id
	 */
	@PutMapping("/salle/{idSalle}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> putSalle(@RequestHeader("xmnr") String idEtablissement,
			@PathVariable String idSalle, @RequestBody SalleDto salleDto) {

		if (salleDto != null) {
			etablissementDto = etabService.getEtablissementById(Long.parseLong(idEtablissement));
			if (etablissementDto != null) {
				salleDto.setEtablissement(etablissementDto);
				salleDto.setIdSalle(Long.parseLong(idSalle));
				salleService.save(salleDto);
				return ResponseEntity.ok("la salle " + idSalle + " a bien été modifiée");
			}
			return ResponseEntity.ok("la salle renseignée ne peux être modifier, veuillez reessayer");
		}
		return ResponseEntity.ok("la salle renseignée ne peux être modifier, veuillez reessayer");
	}

	/**
	 * @author Selim methode de suppression d'une salle, à partir de son id
	 */
	@DeleteMapping("/salle/{idSalle}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteSalle(@PathVariable Long idSalle,
			@RequestHeader("xmnr") String idEtablissement) {

		salleService.removeSalle(idSalle, Long.parseLong(idEtablissement));

		return ResponseEntity.ok("la salle " + idSalle + " a bien été supprimé");
	}

	/**
	 * @author Selim
	 * methode de recherche d'une salle, à partir des critères renseignés par l'utilisateur
	 */
	@PostMapping("/recherche-salle")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> listSalleByCriteria(@RequestBody CriteriaSalle criteria,
			@RequestHeader("xmnr") String idEtablissement) {

		listeSalle = salleService.getSalleByCriteria(criteria, Long.parseLong(idEtablissement));
		
		if(listeSalle != null) {
			
			return ResponseEntity.ok(listeSalle);
		}
		return ResponseEntity.ok("aucune salle disponible ne correspond à vos critères");
	}
	
	
	//Concerne le service Session
	@PostMapping("/find")
	public ResponseEntity<List<SalleDto>> findSallesByCriteria(@RequestBody CriteriaSalle criteria, @RequestHeader String xmnr ){
		System.out.println(criteria);
		List<SalleDto> salles =  salleService.findSalleByCriteria(criteria, Long.valueOf(xmnr));
		return ResponseEntity.ok(salles);
	}


}
