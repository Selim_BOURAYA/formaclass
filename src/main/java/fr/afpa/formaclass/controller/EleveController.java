package fr.afpa.formaclass.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.controller.beans.CriteriaEleve;
import fr.afpa.formaclass.dto.beans.EleveDto;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.service.EleveService;
import fr.afpa.formaclass.service.EtablissementServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/eleves")
public class EleveController {

	@Autowired
	private EleveService eleveService;
	@Autowired
	private EtablissementServiceImpl etabService;
	private EtablissementDto etablissementDto;
	private List<EleveDto> listeEleve;
	private EleveDto eleveDto;
	private Long idEleve;

		
	/**
	 * @author Selim methode d'ajout d'une eleve depuis le formulaire dédié
	 *         (récupération du @RequestBody)
	 */
	@PostMapping("/ajouter-eleve")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> postEleve(@RequestHeader("xmnr") String idEtablissement,
			@RequestBody EleveDto eleveDto) {

		eleveDto.setEtablissement(etabService.getEtablissementById(Long.parseLong(idEtablissement)));
		idEleve = eleveService.save(eleveDto);
		return ResponseEntity.ok("l'élève " + idEleve + " a bien été enregistré(e)");
	}
	
	/**
	 * @author Selim methode de recuperation de la liste des eleves
	 */
	@GetMapping("/liste-eleves")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> affichageList(@RequestHeader("xmnr") String idEtablissement) {

		listeEleve = eleveService.getAllEleve(Long.parseLong(idEtablissement));

		if (listeEleve != null) {
			return ResponseEntity.ok(listeEleve);
		}
		return ResponseEntity.badRequest().body("aucun.e élève n'est enregistré.e");
	}

	/**
	 * @author Selim methode de récupération d'une eleve, à partir de son id
	 */
	@GetMapping("/eleve/{idEleve}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('PROFESSEUR')")
	public ResponseEntity<?> getEleve(@RequestHeader("xmnr") String idEtablissement,
			@PathVariable Long idEleve) {

		eleveDto = eleveService.getEleveById(idEleve, Long.parseLong(idEtablissement));
		if (eleveDto != null) {
			return ResponseEntity.ok(eleveDto);
		}
		return ResponseEntity.ok("l'élève demandé(e) n'existe plus");
	}

	/**
	 * @author Selim methode de modification d'une eleve, à partir de son id
	 */
	@PutMapping("/eleve/{idEleve}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> putEleve(@RequestHeader("xmnr") String idEtablissement,
			@PathVariable Long idEleve, @RequestBody EleveDto eleveDto) {

		if (eleveDto != null) {
			etablissementDto = etabService.getEtablissementById(Long.parseLong(idEtablissement));
			if (etablissementDto != null) {
				
				EleveDto eleveBDD = eleveService.getEleveById(idEleve, Long.parseLong(idEtablissement));
				
				eleveBDD.setNom(eleveDto.getNom());
				eleveBDD.setPrenom(eleveDto.getPrenom());
				eleveBDD.setVoie(eleveDto.getVoie());
				eleveBDD.setCodePostal(eleveDto.getCodePostal());
				eleveBDD.setVille(eleveDto.getVille());
				eleveBDD.setEmail(eleveDto.getEmail());
				
				idEleve = eleveService.save(eleveBDD);
				return ResponseEntity.ok("l'élève " + idEleve + " a bien été modifié");
			}
			return ResponseEntity.ok("l'élève renseigné(e) ne peux être modifier, veuillez reessayer");
		}
		return ResponseEntity.ok("l'élève renseigné(e) ne peux être modifier, veuillez reessayer");
	}

	/**
	 * @author Selim methode de suppression d'une eleve, à partir de son id
	 */
	@DeleteMapping("/eleve/{idEleve}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteEleve(@PathVariable Long idEleve,
			@RequestHeader("xmnr") String idEtablissement) {

		eleveService.removeEleve(idEleve, Long.parseLong(idEtablissement));

		return ResponseEntity.ok("l'élève " + idEleve + " a bien été supprimé(e)");
	}

	//Concerne le service Session
	@PostMapping("/find")
	public ResponseEntity<List<EleveDto>> findElevesByCriteria(@RequestBody CriteriaEleve criteria, @RequestHeader String xmnr ){
		
		List<EleveDto> eleves =  eleveService.findElevesByCriteria(criteria, Long.valueOf(xmnr));
		System.out.println(eleves);
		return ResponseEntity.ok(eleves);
	}
	
	
	
	
	
	
	
	
	

}
