package fr.afpa.formaclass.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.afpa.formaclass.helper.CsvHelper;
import fr.afpa.formaclass.message.ResponseMessage;
import fr.afpa.formaclass.service.CsvService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/csv")
public class CsvController {

  @Autowired
  CsvService fileService;

  @PostMapping(value = {"/upload"})
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
    String message = "";

    if (CsvHelper.hasCSVFormat(file)) {
      try {
        fileService.save(file);

        message = "L'importation du fichier est réussie: " + file.getOriginalFilename();
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
      } catch (Exception e) {
        message = "Impossible d'importer le fichier: " + file.getOriginalFilename() + "!";
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
      }
    }

    message = "Veuillez importer un fichier csv";
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
  }
}