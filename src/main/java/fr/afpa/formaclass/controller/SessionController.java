package fr.afpa.formaclass.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.controller.beans.Session;
import fr.afpa.formaclass.dto.beans.EleveDto;
import fr.afpa.formaclass.dto.beans.ProfesseurDto;
import fr.afpa.formaclass.dto.beans.SalleDto;
import fr.afpa.formaclass.dto.beans.SessionDto;
import fr.afpa.formaclass.dto.beans.StatutDto;
import fr.afpa.formaclass.exception.ResourceNotFoundException;
import fr.afpa.formaclass.service.EleveService;
import fr.afpa.formaclass.service.EtablissementService;
import fr.afpa.formaclass.service.ModeleService;
import fr.afpa.formaclass.service.ProfesseurService;
import fr.afpa.formaclass.service.SessionService;
import fr.afpa.formaclass.service.StatutService;
import fr.afpa.formaclass.service.beans.EStatut;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/session")
public class SessionController {
	
	@Autowired
	SessionService sessServ;
	@Autowired
	ModeleService modServ;
	@Autowired
	EtablissementService etabServ;
	@Autowired
	StatutService statServ;
	@Autowired
	EleveService elServ;
	@Autowired
	ProfesseurService profServ;
	
	
	@PostMapping("/new")
	public  ResponseEntity<SessionDto> create(@RequestBody Session newS, @RequestHeader String xmnr) {
		

		StatutDto statut = statServ.findByDenomination(EStatut.INCOMPLET).get();
		SessionDto newSess = SessionDto.builder().nomination(newS.getNomination()).dateDebut(newS.getDateDebut()).dateFin(newS.getDateFin()).modele(modServ.getModelById(newS.getIdModele(), Long.valueOf(xmnr)).get()).etablissement(etabServ.getEtablissementById(Long.valueOf(xmnr))).statut(statut).build();

		SessionDto good = sessServ.create(newSess);
		good.setListeEleve(new ArrayList<EleveDto>());
		System.out.println("Good"+ good);
		return ResponseEntity.ok(good);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> update( @RequestBody Session newS,  @RequestHeader String xmnr){
		SessionDto updateSession = sessServ.getSession(newS.getIdSession()).get();
		updateSession.setNomination(newS.getNomination());
		updateSession.setDateDebut(newS.getDateDebut());
		updateSession.setDateFin(newS.getDateFin());
		updateSession.setModele(modServ.getModelById(newS.getIdModele(), Long.valueOf(xmnr)).get());
		updateSession.setEtablissement(etabServ.getEtablissementById(Long.valueOf(xmnr)));
		return ResponseEntity.ok(sessServ.update(updateSession));
	}
	
	
	@PutMapping("/cancel/{idSession}")
	public ResponseEntity<SessionDto> cancel(@PathVariable String idSession, @RequestHeader String xmnr ){
		
		SessionDto updateSession = sessServ.getSession(Long.valueOf(idSession)).get();
		updateSession.setStatut(statServ.findByDenomination(EStatut.ANNULE).get());
		return ResponseEntity.ok(sessServ.update(updateSession));
	}
	
	@GetMapping("/{idSession}")
	public ResponseEntity<SessionDto> getSession(@PathVariable String idSession){
		SessionDto sess = sessServ.getSession(Long.valueOf(idSession)).orElseThrow(() -> new ResourceNotFoundException("Cette catégorie n'existe pas"));
		return ResponseEntity.ok(sess);
	}
	
	@GetMapping("/completeSession")
	public  ResponseEntity<List<SessionDto>> getCompleteSession(@RequestHeader String xmnr){
		List<SessionDto> sessList = sessServ.getCompleteSession(Long.valueOf(xmnr));
		return ResponseEntity.ok(sessList);
	}
	
	@GetMapping("/incompleteSession")
	public  ResponseEntity<List<SessionDto>> getIncompleteSession(@RequestHeader String xmnr){
		List<SessionDto> sessList = sessServ.getIncompleteSession(Long.valueOf(xmnr));
		return ResponseEntity.ok(sessList);
	}
	
	@GetMapping("/ended-cancelled-session")
	public  ResponseEntity<List<SessionDto>> getEndedCancelledSession(@RequestHeader String xmnr){
		List<SessionDto> sessList = sessServ.getEndedOrCancelledSession(Long.valueOf(xmnr));
		return ResponseEntity.ok(sessList);
	}
	
	@PutMapping("/{idSession}/add-eleves")
	public ResponseEntity<SessionDto> addEleves(@PathVariable String idSession, @RequestBody List<EleveDto> eleves){
		SessionDto updateSession = sessServ.getSession(Long.valueOf(idSession)).get();
		
		if(!updateSession.getListeEleve().isEmpty()) {
			List<EleveDto> joinList = Stream.concat(eleves.stream(), updateSession.getListeEleve().stream()).collect(Collectors.toList());
			updateSession.setListeEleve(joinList);
			System.out.println("JoinList"+joinList);
		}else {
			
			updateSession.setListeEleve(eleves);
			System.out.println("liste eleve"+eleves);
		}

		updateSession = sessServ.update(updateSession);
		
		System.out.println(eleves);
		eleves.forEach(eleve -> eleve.setEnFormation(true));
		eleves.stream().map(elServ::save).collect(Collectors.toList());
		System.out.println(eleves);
		
		if(sessServ.checkIfSessionIsComplete(Long.valueOf(idSession))) {
			if(updateSession.getDateDebut().isBefore(LocalDate.now())) {
				updateSession.setStatut(statServ.findByDenomination(EStatut.A_VENIR).get());
			}else {
				updateSession.setStatut(statServ.findByDenomination(EStatut.EN_COURS).get());
			}
			updateSession = sessServ.update(updateSession);
		}
		return ResponseEntity.ok(updateSession);
	}
	

	@GetMapping("/{idSession}/delete-eleves/{idEleve}")
	public ResponseEntity<SessionDto> deleteEleves(@RequestHeader String xmnr, @PathVariable String idSession, @PathVariable String idEleve){
		System.out.println(idEleve);
		SessionDto updateSession = sessServ.getSession(Long.valueOf(idSession)).get();
		//List<EleveDto> elevesList = elServ.findEleveMatchWithIdList(idEleves);
		List<EleveDto> newList = updateSession.getListeEleve().stream()
															  .filter( eleve -> Long.valueOf(idEleve) != eleve.getIdEleve())
															  .collect(Collectors.toList());
		EleveDto eleve = elServ.getEleveById(Long.valueOf(idEleve), Long.valueOf(xmnr));
		eleve.setEnFormation(false);
		elServ.save(eleve);
		
		System.out.println(newList);
		updateSession.setListeEleve(newList);
		updateSession = sessServ.update(updateSession);
		
		if((newList.isEmpty() || newList.size() < updateSession.getModele().getEleveMin()) && !"INCOMPLET".equals(updateSession.getStatut())) {
			updateSession.setStatut(statServ.findByDenomination(EStatut.INCOMPLET).get());
			updateSession = sessServ.update(updateSession);
		}
		return ResponseEntity.ok(updateSession);
	}
	
	@PutMapping("/{idSession}/add-professeur")
	public ResponseEntity<SessionDto> addProfesseur(@PathVariable String idSession, @RequestBody ProfesseurDto prof){
		SessionDto updateSession = sessServ.getSession(Long.valueOf(idSession)).get();
		updateSession.setProfesseur(prof);
		updateSession = sessServ.update(updateSession);
		
		if(sessServ.checkIfSessionIsComplete(Long.valueOf(idSession))) {
			if(updateSession.getDateDebut().isBefore(LocalDate.now())) {
				updateSession.setStatut(statServ.findByDenomination(EStatut.A_VENIR).get());
			}else {
				updateSession.setStatut(statServ.findByDenomination(EStatut.EN_COURS).get());
			}
			updateSession = sessServ.update(updateSession);
			
		}

		return ResponseEntity.ok(sessServ.update(updateSession));
	}
	
	@GetMapping("/{idSession}/delete-professeur")
	public ResponseEntity<SessionDto> deleteProfesseur(@PathVariable String idSession){
		SessionDto updateSession = sessServ.getSession(Long.valueOf(idSession)).get();
		updateSession.setProfesseur(null);
		
		if( !"INCOMPLET".equals(updateSession.getStatut())) {
			updateSession.setStatut(statServ.findByDenomination(EStatut.INCOMPLET).get());
		}
		return ResponseEntity.ok(sessServ.update(updateSession));
	}
	
	@PutMapping("/{idSession}/add-salle")
	public ResponseEntity<SessionDto> addSalle(@PathVariable String idSession, @RequestBody SalleDto salle){
		SessionDto updateSession = sessServ.getSession(Long.valueOf(idSession)).get();
		updateSession.setSalle(salle);
		updateSession = sessServ.update(updateSession);
		if(sessServ.checkIfSessionIsComplete(Long.valueOf(idSession))) {
			if(updateSession.getDateDebut().isBefore(LocalDate.now())) {
				updateSession.setStatut(statServ.findByDenomination(EStatut.A_VENIR).get());
			}else {
				updateSession.setStatut(statServ.findByDenomination(EStatut.EN_COURS).get());
			}
			updateSession = sessServ.update(updateSession);
			
		}
		return ResponseEntity.ok(updateSession);
	}
	
	@GetMapping("/{idSession}/delete-salle")
	public ResponseEntity<SessionDto> deleteSalle(@PathVariable String idSession){
		SessionDto updateSession = sessServ.getSession(Long.valueOf(idSession)).get();
		updateSession.setSalle(null);
		if( !"INCOMPLET".equals(updateSession.getStatut())) {
			updateSession.setStatut(statServ.findByDenomination(EStatut.INCOMPLET).get());
		}
		return ResponseEntity.ok(sessServ.update(updateSession));
	}
	
	@GetMapping("/professeur-session/{idProfesseur}")
	public ResponseEntity<SessionDto> getActualSessionProfessor (@RequestHeader("xmnr") String idEtablissement,
	@PathVariable Long idProfesseur) {
				
		return ResponseEntity.ok(sessServ.findActualSession(idProfesseur));
	}
	
	@GetMapping("/professeur-listsessions/{idProfesseur}")
	public ResponseEntity <List<SessionDto>> getSessionProfessor (@RequestHeader("xmnr") String idEtablissement,
	@PathVariable Long idProfesseur) {
				
		return ResponseEntity.ok(sessServ.findListSessionByIdProfesseur(idProfesseur));
	}
	
	@GetMapping("/eleve-session/{idEleve}")
	public ResponseEntity<SessionDto> getActualSessionEleve (@RequestHeader("xmnr") String idEtablissement,
	@PathVariable Long idEleve) {
				
		return ResponseEntity.ok(sessServ.findActualSessionEleve(idEleve));
	}
	
	@GetMapping("/eleve-listesession/{idEleve}")
	public ResponseEntity <List<SessionDto>> getSessionEleve (@RequestHeader("xmnr") String idEtablissement,
	@PathVariable Long idEleve) {
				
		return ResponseEntity.ok(sessServ.findListSessionByIdEleve(idEleve));
	}
	
	//findEleve
	//findSalle
	//findProfesseur

}
