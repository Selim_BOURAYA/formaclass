package fr.afpa.formaclass.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.dto.beans.EquipementDto;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.service.EquipementServiceImpl;
import fr.afpa.formaclass.service.EtablissementServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/equipement")
public class EquipementController {
	
	@Autowired
	private EquipementServiceImpl equipementService;
	@Autowired
	private EtablissementServiceImpl etabService;
	private EtablissementDto etablissementDto;
	private List<EquipementDto> listeEquipement;
	private EquipementDto equipementDto;
	private Long idEquipement;
	
	/**
	 * Méthode du controller permettant de recuperer la liste de l'ensemble des equipements de l'etablissement
	 */
	@GetMapping("/liste-equipements")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getListeEquipements(@RequestHeader("xmnr") String idEtabliseement){
		
		listeEquipement = equipementService.getAllMateriel(Long.parseLong(idEtabliseement));
		if(listeEquipement != null) {
			return ResponseEntity.ok(listeEquipement);
		}
		return ResponseEntity.badRequest().body("Aucun equipement n'est enregistré pour le moment");
	}
	
	/**
	 * Méthode du controller permettant d'afficher un equipement à partir de son id
	 */
	@GetMapping("/equipement/{idEquipement}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getEquipement(@RequestHeader("xmnr") String idEtablissement, @PathVariable Long idEquipement){
		
		equipementDto = equipementService.getEquipementById(idEquipement, Long.parseLong(idEtablissement));
		if(equipementDto != null) {
			return ResponseEntity.ok(equipementDto);
		}
		return ResponseEntity.ok("Equipement non disponible");	
	}
	
	
	/**
	 * Méthode du controller permettant d'ajouter un nouvel equipement
	 */
	@PostMapping("/ajouter-equipement")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> addEquipement(@RequestHeader("xmnr") String idEtablissement, @RequestBody EquipementDto equipementDto){
		
		equipementDto.setEtablissementDto(etabService.getEtablissementById(Long.parseLong(idEtablissement)));
		idEquipement = equipementService.save(equipementDto);
		return ResponseEntity.ok("L'equipement a bien été ajouté");
	}
	
	/**
	 * Méthode du controller permettant de supprimer un equipement à partir de son id
	 */
	@DeleteMapping("/equipement/{idEquipement}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteEquipement(@PathVariable Long IdEquipement, @RequestHeader("xmnr") String idEtablissement){
		
		equipementService.deleteEquipement(idEquipement, Long.parseLong(idEtablissement));
		return ResponseEntity.ok("L'equipement a bien été supprimé");
	}
	
	/**
	 * Méthode du controller permettant de mettre à jour un equipement à partir de son id
	 */
	@PutMapping("/equipement/{idEquipement}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> updateProfesseur(@RequestHeader("xmnr") String idEtablissement, @PathVariable Long idEquipement, @RequestBody EquipementDto equipementDto){
		
		if (equipementDto != null) {
			etablissementDto = etabService.getEtablissementById(Long.parseLong(idEtablissement));
			if(etablissementDto != null) {
				equipementDto.setEtablissementDto(etablissementDto);
				idEquipement = equipementService.save(equipementDto);
				return ResponseEntity.ok("L'equipement a été modifié avec succès");
			}
			return ResponseEntity.ok("L'equipement selectionné ne peut pas être modifié");
		}
		return ResponseEntity.ok("L'equipement selectionné ne peut pas être modifié");
	}
	
}