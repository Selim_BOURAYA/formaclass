package fr.afpa.formaclass.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.dto.beans.AllocationEquipementDto;
import fr.afpa.formaclass.dto.beans.EquipementDto;
import fr.afpa.formaclass.dto.beans.SalleDto;
import fr.afpa.formaclass.service.AllocationEquipementServiceImpl;
import fr.afpa.formaclass.service.EquipementService;
import fr.afpa.formaclass.service.EtablissementService;
import fr.afpa.formaclass.service.EtablissementServiceImpl;
import fr.afpa.formaclass.service.SalleServiceImpl;

import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/allocationEquipement")
public class AllocationEquipementController {
/*
	@Autowired
	private AllocationEquipementServiceImpl allocEquService;
	@Autowired
	private EtablissementServiceImpl etabServ;
	private AllocationEquipementDto allocationEquipementDto;
	private SalleDto salleDto;
	private SalleServiceImpl salleService;
	private EquipementDto equipementDto;
	private EquipementService equipService;
	private List<AllocationEquipementDto> listAllocEquipement;
	private Long idAllocationEquipement;
	*/
	
	/**
	 * Méthode du controller permettant de recuperer la liste de l'ensemble des allocations d'une salle specifique
	 */
	/*@GetMapping("/liste-allocations-equipement/{idSalle}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAllocationEquipementBySalle(@RequestHeader("xmnr") String idEtablissement, @PathVariable String idSalle){
		
		listAllocEquipement = allocEquService.getAllocationEquipementBySalleId(Long.parseLong(idSalle));
		if(listAllocEquipement != null) {
			return ResponseEntity.ok(listAllocEquipement);
		}
		return ResponseEntity.badRequest().body("Il n'y a pas encore d'allocation d'équipement pour cette salle");
	}
	*/
	
	/**
	 * Méthode du controller permettant d'afficher une allocation equipement à partir de son id
	 */
	/*@GetMapping("/equipement/{idAllocationEquipement}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> getAllocationEquipement(@RequestHeader("xmnr") String idEtablissement, @PathVariable Long idAllocationEquipement){
		
		allocationEquipementDto = allocEquService.getAllocationById(idAllocationEquipement);
		if(allocationEquipementDto != null) {
			return ResponseEntity.ok(allocationEquipementDto);
		}
		return ResponseEntity.ok("Allocation d'equipement non disponible");	
	}
	*/
	
	/**
	 * Méthode du controller permettant d'ajouter une nouvelle allocation d'equipement
	 */
	/*@PostMapping("/ajouter-allocationEquipement")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> addAllocationEquipement(@RequestHeader("xmnr") String idEtablissement, @RequestBody AllocationEquipementDto allocationEquipementDto){
		
		idAllocationEquipement = allocEquService.save(allocationEquipementDto);
		return ResponseEntity.ok("L'allocation d'équipement a bien été créée");	
	}
	*/
}