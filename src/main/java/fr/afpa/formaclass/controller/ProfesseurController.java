package fr.afpa.formaclass.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.controller.beans.CriteriaProfesseur;
import fr.afpa.formaclass.controller.beans.ProfesseurInscription;
import fr.afpa.formaclass.dto.beans.AuthUserDto;
import fr.afpa.formaclass.dto.beans.BasicUser;
import fr.afpa.formaclass.dto.beans.EtablissementDto;
import fr.afpa.formaclass.dto.beans.ProfesseurDto;
import fr.afpa.formaclass.dto.beans.RoleDto;
import fr.afpa.formaclass.security.jwt.JwtUtils;
import fr.afpa.formaclass.service.AuthUserService;
import fr.afpa.formaclass.service.EtablissementServiceImpl;
import fr.afpa.formaclass.service.ProfesseurServiceImpl;
import fr.afpa.formaclass.service.RoleService;
import fr.afpa.formaclass.service.beans.ERole;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/professeurs")
public class ProfesseurController {
		
	@Autowired
	private ProfesseurServiceImpl professeurService;
	@Autowired
	private EtablissementServiceImpl etabService;
	@Autowired
	private AuthUserService authUserService;
	@Autowired
	private PasswordEncoder encoder;
	@Autowired
	private RoleService roleService;
	private EtablissementDto etablissementDto;
	private List<ProfesseurDto> listeProfesseur;
	private ProfesseurDto professeurDto;
	private Long idProfesseur;
	private AuthUserDto authUser;
	private Set<RoleDto> roles;
	@Autowired
	private JwtUtils jwtUtils;

	
	/**
	 * @author Selim methode d'ajout d'une professeur depuis le formulaire dédié
	 *         (récupération du @RequestBody)
	 */
	@PostMapping("/ajouter-professeur")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> postProfesseur(@RequestHeader("xmnr") String idEtablissement,
			@RequestBody ProfesseurInscription inscription) {
		
		Map<String, Boolean> error = new HashMap<>();
		if(authUserService.existsByEmail(inscription.getAuthUser().getEmail())) {
			error.put("userIsExist", true);	
		}
		if(!error.isEmpty()) {
			return ResponseEntity.ok()
					.body(error);
		}
		
		authUser = new AuthUserDto(inscription.getAuthUser().getEmail(), encoder.encode(inscription.getAuthUser().getPassword()));
		Optional <RoleDto> userRole = roleService.findByName(ERole.ROLE_PROFESSEUR);
		
		if(userRole != null) {
			
			roles = new HashSet<>();
			authUser.setRoles(roles);
		}
		
		// Implementation du basicUser qui va servir a définir le token et a être envoyé dans le service mail
		BasicUser basicUser = BasicUser.builder()
									   .email(inscription.getAuthUser().getEmail())
									   .nom(inscription.getNom())
									   .prenom(inscription.getPrenom())
									   .build();
		
		String token = jwtUtils.generateJwtTokenActivationCompte(basicUser);
		basicUser.setToken(token);
		authUser.setTokenActivationCompte(token);
		
		ProfesseurDto professeurDto = ProfesseurDto.builder() 
										.nom(inscription.getNom())
										.prenom(inscription.getPrenom())
										.telephone(inscription.getTelephone())
										.voie(inscription.getVoie())
										.ville(inscription.getVille())
										.codePostal(inscription.getCodePostal())
										.authUser(authUser)
										.etablissement(null)
										.build();
		
		professeurDto.setEtablissement(etabService.getEtablissementById(Long.parseLong(idEtablissement)));

		idProfesseur = professeurService.save(professeurDto);
		return ResponseEntity.ok("la/le professeur " + idProfesseur + " a bien été enregistrée");
	}
	
	/**
	 * @author Selim methode de recuperation de la liste des professeurs
	 */
	@GetMapping("/liste-professeurs")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> affichageListe(@RequestHeader("xmnr") String idEtablissement) {

		listeProfesseur = professeurService.getAllProfesseur(Long.parseLong(idEtablissement));

		if (listeProfesseur != null) {
			return ResponseEntity.ok(listeProfesseur);
		}
		return ResponseEntity.badRequest().body("aucun(e) professeur n'est enregistrée");
	}

	/**
	 * @author Selim methode de récupération d'une professeur, à partir de son id
	 */
	@GetMapping("/professeur/{idProfesseur}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('PROFESSEUR')")
	public ResponseEntity<?> getProfesseur(@RequestHeader("xmnr") String idEtablissement,
			@PathVariable Long idProfesseur) {

		professeurDto = professeurService.getProfesseurById(idProfesseur, Long.parseLong(idEtablissement));
		if (professeurDto != null) {
			return ResponseEntity.ok(professeurDto);
		}
		return ResponseEntity.ok("la/le professeur demandé(e) n'existe plus");
	}

	/**
	 * @author Selim methode de modification d'une professeur, à partir de son id
	 */
	@PutMapping("/professeur/{idProfesseur}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> putProfesseur(@RequestHeader("xmnr") String idEtablissement,
			@PathVariable Long idProfesseur, @RequestBody ProfesseurDto professeurDto) {

		if (professeurDto != null) {
			etablissementDto = etabService.getEtablissementById(Long.parseLong(idEtablissement));
			if (etablissementDto != null) {
				
				ProfesseurDto professeurBDD = professeurService.getProfesseurById(idProfesseur, Long.parseLong(idEtablissement));
				
				professeurBDD.setNom(professeurDto.getNom());
				professeurBDD.setPrenom(professeurDto.getPrenom());
				professeurBDD.setVoie(professeurDto.getVoie());
				professeurBDD.setCodePostal(professeurDto.getCodePostal());
				professeurBDD.setVille(professeurDto.getVille());
				professeurBDD.getAuthUser().setEmail(professeurDto.getAuthUser().getEmail());
				
				idProfesseur = professeurService.save(professeurBDD);
				return ResponseEntity.ok("la/le professeur " + idProfesseur + " a bien été modifié(e)");
			}
			return ResponseEntity.ok("la/le professeur renseigné(e) ne peux être modifier, veuillez reessayer");
		}
		return ResponseEntity.ok("la/le professeur renseigné(e) ne peux être modifier, veuillez reessayer");
	}

	/**
	 * @author Selim methode de suppression d'une professeur, à partir de son id
	 */
	@DeleteMapping("/professeur/{idProfesseur}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteProfesseur(@PathVariable Long idProfesseur,
			@RequestHeader("xmnr") String idEtablissement) {

		professeurService.removeProfesseur(idProfesseur, Long.parseLong(idEtablissement));

		return ResponseEntity.ok("la/le professeur " + idProfesseur + " a bien été supprimé(e)");
	}

	/**
	 * @author Selim
	 * methode de recherche d'une professeur, à partir des critères renseignés par l'utilisateur
	 */
	@PostMapping("/recherche-professeur")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> listProfesseurByCriteria(@RequestBody CriteriaProfesseur criteria,
			@RequestHeader("xmnr") String idEtablissement) {

		//listeProfesseur = professeurService.getProfesseurByCriteria(criteria, Long.parseLong(idEtablissement));
		
		if(listeProfesseur != null) {
			
			return ResponseEntity.ok(listeProfesseur);
		}
		return ResponseEntity.ok("aucun(e) professeur disponible ne correspond à vos critères");
	}
	
	
	
	/**
	 * Récupère un liste de professeur en fonction des critères donnés dans critériaProfesseur
	 * @param criteria
	 * @param xmnr
	 * @return
	 */
	//Concerne le service Session
	@PostMapping("/find")
	public ResponseEntity<List<ProfesseurDto>> findProfsByCriteria(@RequestBody CriteriaProfesseur criteria, @RequestHeader String xmnr ){
		
		List<ProfesseurDto> profs =  professeurService.findProfesseurByCriteria(criteria, Long.valueOf(xmnr));
		System.out.println(profs);
		return ResponseEntity.ok(profs);
	}

}
