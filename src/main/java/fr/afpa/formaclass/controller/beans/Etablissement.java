package fr.afpa.formaclass.controller.beans;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class Etablissement {

	private String nom;
	
	private String voie;
	
	private String ville;
	
	private String codePostal;
	
	private String email;
	
	private String telephone;
}
