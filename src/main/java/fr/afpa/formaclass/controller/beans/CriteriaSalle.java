package fr.afpa.formaclass.controller.beans;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@Builder
@ToString
public class CriteriaSalle {
	
	private LocalDate debutSession;
	private LocalDate finSession;
	private boolean accessibilite;
	private int capaciteMax;

}
