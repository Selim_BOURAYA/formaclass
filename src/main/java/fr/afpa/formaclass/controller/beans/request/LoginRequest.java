package fr.afpa.formaclass.controller.beans.request;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class LoginRequest {
	
	private String email;
	private String password;
}
