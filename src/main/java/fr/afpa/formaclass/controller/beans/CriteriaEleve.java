package fr.afpa.formaclass.controller.beans;

import java.time.LocalDate;

import lombok.Getter;

@Getter
public class CriteriaEleve {

	Long idModele;
	LocalDate dateDebut;
	LocalDate dateFin;
	
}
