package fr.afpa.formaclass.controller.beans;

import java.time.LocalDate;

import lombok.Getter;

@Getter
public class CriteriaProfesseur {

	Long idCategorie;
	LocalDate dateDebut;
	LocalDate dateFin;
	
}

	