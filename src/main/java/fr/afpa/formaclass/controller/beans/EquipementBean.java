package fr.afpa.formaclass.controller.beans;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString

public class EquipementBean {

	private Long idEquipement;
	@NotBlank(message = "Champ obligatoire") @Size(min = 2, max = 30, message="La taille du nom doit être comprise entre 2 et 30 caractères")
	private String nom;
	private boolean disponible;
	@NotBlank(message = "Champ obligatoire")
	private int quantiteTotale;
	@NotBlank(message = "Champ obligatoire")
	private int quantiteAllouee;
	
}