package fr.afpa.formaclass.controller.beans;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
public class AuthUser {

	@NotNull
	@Email
	private String email;
	
	@NotNull
	private String password;
	
	private Set<String> role;
}
