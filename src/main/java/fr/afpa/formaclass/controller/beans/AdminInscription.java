package fr.afpa.formaclass.controller.beans;

import java.util.Date;
import java.util.Set;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@AllArgsConstructor @NoArgsConstructor
@ToString
public class AdminInscription {

	private AuthUser authUser;
	
	private Etablissement etablissement;
	
	private String nom;
	
	private String prenom;
	
	private String voie;
	
	private String ville;
	
	private String codePostal;
	
	private String telephone;
	
	
	
	
}
