package fr.afpa.formaclass.controller.beans;

import java.time.LocalDate;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Session {
	
	private Long idSession;
	
	private String nomination;
	
	private LocalDate dateDebut;
	
	private LocalDate dateFin;
		
	private Long idModele;
	

}
