package fr.afpa.formaclass.controller.beans.request;

import lombok.Getter;
import lombok.ToString;

@Getter

public class PasswordRequest {
	private String password;
}
