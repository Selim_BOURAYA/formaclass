package fr.afpa.formaclass.controller.beans;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class AllocationEquipementBean {

	private Long idAllocationEquipement;
	@NotBlank(message = "Champ obligatoire")
	private int quantite;
	private Long idEquipement;
	private Long idSalle;
	
}