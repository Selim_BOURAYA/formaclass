package fr.afpa.formaclass.controller.beans;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString

public class Modele {
	
	private Long idModele;

	private String titre;
	
	private int eleveMin;
	
	private int eleveMax;
	
	private String description;
	
	private Long idCategorie;

}
