package fr.afpa.formaclass.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.controller.beans.response.MessageResponse;
import fr.afpa.formaclass.dto.beans.CategorieDto;
import fr.afpa.formaclass.exception.ResourceNotFoundException;
import fr.afpa.formaclass.service.CategoryService;
import fr.afpa.formaclass.service.EtablissementService;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/category")
@PreAuthorize("hasRole('ADMIN')")
public class CategoryController {

	@Autowired
	private CategoryService catServ;
	@Autowired
	private EtablissementService etabServ;
	
	@GetMapping("/all")
	public List<CategorieDto> getAllCategory(@RequestHeader String xmnr ){
		return catServ.getAllCategory(Long.valueOf(xmnr));
	}
	
	@PostMapping("/new")
	public  List<CategorieDto> create(@RequestBody CategorieDto newCat, @RequestHeader String xmnr) {
		System.out.println(newCat);
		newCat.setEtablissement(etabServ.getEtablissementById(Long.valueOf(xmnr)));
		return catServ.save(newCat, Long.valueOf(xmnr));
	}
	
	@GetMapping("/{idCat}")
	public ResponseEntity<CategorieDto> getCategoryById(@PathVariable String idCat, @RequestHeader String xmnr) {	
		CategorieDto cat = catServ.getCategoryById(Long.valueOf(idCat), Long.valueOf(xmnr)).orElseThrow(() -> new ResourceNotFoundException("Cette catégorie n'existe pas"));
		return ResponseEntity.ok(cat);
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateCategory( @RequestBody CategorieDto cat,  @RequestHeader String xmnr){
		System.out.println(cat);
		List<CategorieDto> newCat = catServ.updateCategory(cat, xmnr);
		if(newCat == null) {
			return ResponseEntity.badRequest().body(MessageResponse.builder().message("Il n'est pas possible de modifier cette catégorie").build());
		} else {
			return ResponseEntity.ok(newCat);
		}
		
		
	}
	
	@DeleteMapping("/delete/{idCategorie}")
	public ResponseEntity<?> deleteCategory(@PathVariable String idCategorie, @RequestHeader String xmnr){
		Map<String, Boolean> response = new HashMap<>();
		
		List<CategorieDto> list = catServ.deleteCategory(Long.valueOf(idCategorie), Long.valueOf(xmnr));
		System.out.println(list);
		if(list != null){
			return ResponseEntity.ok(list);
		}else {
			return ResponseEntity.badRequest().body("error machin");
		}

	}
	
	
	
	
	
	
	
	
	
}
