package fr.afpa.formaclass.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.formaclass.controller.beans.Modele;
import fr.afpa.formaclass.dto.beans.ModeleDto;
import fr.afpa.formaclass.exception.ResourceNotFoundException;
import fr.afpa.formaclass.service.CategoryService;
import fr.afpa.formaclass.service.EtablissementService;
import fr.afpa.formaclass.service.ModeleService;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/modele")
@PreAuthorize("hasRole('ADMIN')")
public class ModeleController {
	
	@Autowired
	ModeleService modServ;
	
	@Autowired
	EtablissementService etabServ;
	
	@Autowired 
	CategoryService catServ;
	
	
	/**
	 * Créé un modele
	 * @param newM
	 * @param xmnr
	 * @return
	 */
	@PostMapping("/new")
	public  ModeleDto create(@RequestBody Modele newM, @RequestHeader String xmnr) {
		
		System.out.println(newM);
		ModeleDto newMod = ModeleDto.builder().description(newM.getDescription()).titre(newM.getTitre()).eleveMax(newM.getEleveMax()).eleveMin(newM.getEleveMin()).categorie(catServ.getCategoryById(newM.getIdCategorie(), Long.valueOf(xmnr)).get()).build();
		newMod.setEtablissement(etabServ.getEtablissementById(Long.valueOf(xmnr)));
		return modServ.createModel(newMod, Long.valueOf(xmnr));
	}
	
	/**
	 * Modifie un modele
	 * @param newM
	 * @param xmnr
	 * @return
	 */
	@PutMapping("/update")
	public ResponseEntity<?> update( @RequestBody Modele newM,  @RequestHeader String xmnr){
		System.out.println("gogo"+newM);
		ModeleDto updateMod = modServ.getModelById(newM.getIdModele(), Long.valueOf(xmnr)).orElseThrow(() -> new ResourceNotFoundException("Cette catégorie n'existe pas"));
		updateMod.setCategorie(catServ.getCategoryById(newM.getIdCategorie(), Long.valueOf(xmnr)).get());
		updateMod.setDescription(newM.getDescription());
		updateMod.setTitre(newM.getTitre());
		updateMod.setEleveMax(newM.getEleveMax());
		updateMod.setEleveMin(newM.getEleveMin());
		ModeleDto newMod = modServ.updateModel(updateMod);

			return ResponseEntity.ok(modServ.getAllModel(Long.valueOf(xmnr)));
	}
	
	
	/**
	 * Récupère un modele grace a l'id donnée en param
	 * @param idMod
	 * @param xmnr
	 * @return
	 */
	@GetMapping("/{idModele}")
	public ResponseEntity<ModeleDto> getModele(@PathVariable String idModele, @RequestHeader String xmnr) {	
		System.out.println("id Modele : "+idModele);
		ModeleDto mod = modServ.getModelById(Long.valueOf(idModele), Long.valueOf(xmnr)).orElseThrow(() -> new ResourceNotFoundException("Cette catégorie n'existe pas"));
		return ResponseEntity.ok(mod);
	}
	
	
	/**
	 * Permet de supprimer un modele s'il n'est pas lié a des sessions
	 * @param idMod
	 * @param xmnr
	 * @return
	 */
	@DeleteMapping("/delete/{idModele}")
	public ResponseEntity<Map<String, Boolean>> delete(@PathVariable String idModele, @RequestHeader String xmnr){
		Map<String, Boolean> response = new HashMap<>();
		System.out.println(idModele);
		if(modServ.deleteModel(Long.valueOf(idModele),  Long.valueOf(xmnr))){
			response.put("deleted", Boolean.TRUE);
		}else {
			response.put("deleted", Boolean.FALSE);
		}
		return ResponseEntity.ok(response);
	}
	
	
	@GetMapping("/allModeles")
	public ResponseEntity<List<ModeleDto>> getAllModeles(@RequestHeader String xmnr){
		System.out.println("jojo");
		return ResponseEntity.ok(modServ.getAllModel(Long.valueOf(xmnr)));
	}
	

}
