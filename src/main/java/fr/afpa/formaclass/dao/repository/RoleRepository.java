package fr.afpa.formaclass.dao.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.formaclass.dao.beans.RoleDao;
import fr.afpa.formaclass.service.beans.ERole;

public interface RoleRepository extends JpaRepository<RoleDao, Long> {
	
	Optional<RoleDao> findByName(ERole name);

}
