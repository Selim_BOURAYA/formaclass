package fr.afpa.formaclass.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.formaclass.dao.beans.EleveDao;

public interface EleveRepository extends JpaRepository<EleveDao, Long> {

	@Query("select e from EleveDao e where e.idEleve = ?1 and e.etablissement.idEtablissement = ?2")
	EleveDao findByIdEleveAndEtablissement(Long idEleve, Long idEtablissement);
	@Query("select e from EleveDao e where e.etablissement.idEtablissement = ?1")
	List <EleveDao> findByIdEtablissement(Long idEtablissement);
	EleveDao findByEmail(String email);
	boolean existsByEmail(String email);
	
	//Concerne le Service Session
	
	/**
	 * Permet de récupérer des eleves en fonction de la liste d'id indiquée en param
	 * @param idEleve
	 * @return
	 */
	List<EleveDao> findByIdEleveIn(List<Long> idEleve);
	
	
	
	/**
	 * Permet de récupérer les eleves par les critères indiqués en param 
	 * @param idCategory
	 * @param dateDebut
	 * @param dateFin
	 * @param idEtablissement
	 * @return
	 */
	
	@Query("FROM EleveDao e JOIN e.listeFormationSouhaitees s  WHERE s.idModele = ?1  and  e.etablissement.idEtablissement = ?2 and e.enFormation = false and e.idEleve not in ( select e2.idEleve FROM EleveDao e2 JOIN e2.listeSessionEffectuees sef INNER JOIN SessionDao s2 ON s2.idSession = sef.idSession INNER JOIN ModeleDao md2 ON s2.modele.idModele = md2.idModele where md2.idModele = ?1) " )
	List<EleveDao> findEleveByCriteria(Long idModele,/* LocalDate dateDebut, LocalDate dateFin, */Long idEtablissement);
	

}
