package fr.afpa.formaclass.dao.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.formaclass.dao.beans.StatutDao;
import fr.afpa.formaclass.service.beans.EStatut;

public interface StatutRepository extends JpaRepository<StatutDao, Long> {
	
	Optional<StatutDao> findByNomination(EStatut nomination);

}
