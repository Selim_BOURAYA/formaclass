package fr.afpa.formaclass.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.afpa.formaclass.dao.beans.SessionDao;

@Repository
public interface SessionRepository extends JpaRepository<SessionDao, Long> {
	
	
	@Query("FROM SessionDao s INNER JOIN StatutDao stat ON s.statut.idStatut = stat.idStatut where stat.nomination like 'EN_COURS' or stat.nomination like 'A_VENIR' and s.etablissement.idEtablissement = ?1 ")
	List<SessionDao> findCompleteSession(Long idEtablissement);
	
	@Query("FROM SessionDao s INNER JOIN StatutDao stat ON s.statut.idStatut = stat.idStatut where stat.nomination like 'INCOMPLET' and  s.etablissement.idEtablissement = ?1 ")
	List<SessionDao> findIncompleteSession(Long idEtablissement);

	@Query("FROM SessionDao s INNER JOIN StatutDao stat ON s.statut.idStatut = stat.idStatut where stat.nomination like 'ANNULE' or stat.nomination like 'TERMINE' and s.etablissement.idEtablissement = ?1 ")
	List<SessionDao> findEndedOrCancelledSession(Long idEtablissement);
	
	/**
	 * Verifie si la session est complete ou non 
	 * @param idSession
	 * @return
	 */
	@Query("select case when count(c) > 0 then true else false end from SessionDao c INNER JOIN ModeleDao m ON c.modele.idModele = m.idModele  WHERE c.idSession = ?1 and c.salle is not null and c.professeur is not null and m.eleveMin < ( select count(e) from SessionDao sd JOIN sd.listeEleve e WHERE sd.idSession = ?1 ) ")
	boolean checkIfSessionIsComplete(Long idSession);
	
	/**
	 * @author Selim
	 * requête récupérant la session actuelle, au statut en cours, d'un professeur
	 * @param idProfesseur
	 * @return
	 */
	@Query("FROM SessionDao s INNER JOIN StatutDao stat ON s.statut.idStatut = stat.idStatut where stat.nomination like 'EN_COURS' and s.professeur.idProfesseur = ?1 ")
	SessionDao findActualSessionByIdProfessor(Long idProfesseur);
	
	/**
	 * @author Selim
	 * requête récupérant la liste des sessions animées par un professeur
	 * @param idProfesseur
	 * @return
	 */
	@Query("FROM SessionDao s where s.professeur.idProfesseur = ?1 ")
	List<SessionDao> findSessionByIdProfessor(Long idProfesseur);
	
	/**
	 * @author Selim
	 * requête récupérant la session actuelle, au statut en cours, d'un élève
	 * @param idProfesseur
	 * @return
	 */
	@Query("FROM SessionDao s INNER JOIN s.listeEleve l WHERE l.idEleve = ?1")
	SessionDao findActualSessionByIdEleve(Long idEleve);
	
	/**
	 * @author Selim
	 * requête récupérant la liste des sessions suivies par un élève
	 * @param idProfesseur
	 * @return
	 */
//	//@Query("FROM SessionDao s INNER JOIN EleveDao e ON e.listeSessionEffectuees.listeEleve.idEleve = s.listeEleve.idEleve WHERE e.listeSessionEffectuees.idEleve = ?1")
//	@Query("FROM SessionDao s JOIN EleveDao e INNER JOIN s.listeEleve l ON l.idEleve = e.listeSessionEffectuees.listeEleve.idEleve WHERE e.listeSessionEffectuees.listeEleve.idEleve = ?1")
//
//	List<SessionDao> findSessionByEleve(Long idELeve);
}
