package fr.afpa.formaclass.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.formaclass.dao.beans.AllocationEquipementDao;

public interface AllocationEquipementRepository extends JpaRepository<AllocationEquipementDao, Long>{

	/**
	 * Requete HQL permettant de retourner une liste d'allocations materiels par l'id de salle souhaitée
	 */
	@Query("select a from AllocationEquipementDao a where a.salle.idSalle = ?1")
	List<AllocationEquipementDao> getAllocationEquipementBySalleId(Long idSalle);
	
	/**
	 * Requete HQL permettant de retourner une liste d'allocations matériels par l'id du matériel recherché
	 */
	@Query("select a from AllocationEquipementDao a where a.equipement.idEquipement = ?1")
	List<AllocationEquipementDao> getAllocationEquipementByEquipementId(Long idEquipement);

}