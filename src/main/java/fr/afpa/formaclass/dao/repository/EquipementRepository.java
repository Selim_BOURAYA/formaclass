package fr.afpa.formaclass.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.formaclass.dao.beans.EquipementDao;

public interface EquipementRepository extends JpaRepository<EquipementDao, Long> {

	
	/**
	* Requete HQL permettant de retourner une liste d'equipements par etablissement (ici l'id de l'etablissement sera 1)
	*/
	@Query("select e from EquipementDao e where e.etablissement.idEtablissement = ?1")
	List<EquipementDao> findByIdEtablissement(Long idEtablissement);
	
	/**
	* Requete HQL permettant de retourner une liste d'equipements disponible par etablissement (ici l'id de l'etablissement sera 1)
	*/
	@Query("select e.quantiteTotale - e.quantiteAllouee from EquipementDao e where e.etablissement.idEtablissement = ?1" )
	List<EquipementDao> getMaterielDisponibleByIdEtablissement(Long IdEtablissement);

	/**
	* Requete HQL permettant de retourner un equipement selon son id
	* Le premier parametre sera l'equipement (?1) et le second sera l'etablissement (?2)
	* => sera utilisee pour supprimer l'equipement specifie
	*/
	@Query("select e from EquipementDao e where e.idEquipement = ?1 and e.etablissement.idEtablissement = ?2")
	EquipementDao findEquipementByIdEquipement(Long idEquipement, Long idEtablissement);
}