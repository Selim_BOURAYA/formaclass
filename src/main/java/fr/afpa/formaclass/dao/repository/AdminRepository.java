package fr.afpa.formaclass.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.formaclass.dao.beans.AdminDao;

public interface AdminRepository extends JpaRepository<AdminDao, Long> {

}
