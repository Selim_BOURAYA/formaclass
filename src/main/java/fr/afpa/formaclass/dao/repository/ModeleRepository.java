package fr.afpa.formaclass.dao.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.formaclass.dao.beans.ModeleDao;

public interface ModeleRepository extends JpaRepository<ModeleDao, Long> {

	
	@Query("FROM ModeleDao m WHERE m.etablissement.idEtablissement = ?1")
	List<ModeleDao> findAllByIdEtablissement(Long idEtab);

	@Query(" FROM ModeleDao m WHERE m.idModele = ?1 and m.etablissement.idEtablissement = ?2")
	Optional<ModeleDao> findByIdModeleAndIdEtablissement(Long idModel, Long idEtab);
	
	
	@Query("select case when count(s) > 0 then true else false end FROM SessionDao s where s.modele.idModele = ?1 and s.etablissement.idEtablissement = ?2")
	boolean ifAnySessionExtendsOfThisModel(Long idModele, Long idEtablissement);

}
