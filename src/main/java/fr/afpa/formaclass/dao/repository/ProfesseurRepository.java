package fr.afpa.formaclass.dao.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import fr.afpa.formaclass.dao.beans.ProfesseurDao;


public interface ProfesseurRepository extends JpaRepository<ProfesseurDao, Long> {

	@Query("select p from ProfesseurDao p where p.idProfesseur = ?1 and p.etablissement.idEtablissement = ?2")
	ProfesseurDao findByIdProfesseurAndEtablissement(Long idProfesseur, Long idEtablissement);
	@Query("select p from ProfesseurDao p where p.etablissement.idEtablissement = ?1")
	List <ProfesseurDao> findByIdEtablissement(Long idEtablissement);
	@Query("select p from ProfesseurDao p where p.authUser.email = ?1")
	List <ProfesseurDao> findByEmail(String email);

	
	
	// Concerne le service Session
	//@Query("FROM ProfesseurDao p INNER JOIN SessionDao s ON s.professeur.idProfesseur = p.idProfesseur WHERE s.dateDebut not between ?3 and ?4 and s.dateFin not between ?3 and ?4 and p.categorie.idCategorie = ?2 and p.etablissement.idEtablissement = ?1")
	@Query("FROM ProfesseurDao p WHERE p.categorie.idCategorie = ?2 and p.etablissement.idEtablissement = ?1 and p.idProfesseur not in (select p2.idProfesseur FROM ProfesseurDao p2 INNER JOIN SessionDao s2 ON s2.professeur.idProfesseur = p2.idProfesseur WHERE s2.dateDebut between ?3 and ?4 or s2.dateFin between ?3 and ?4) ")
	List<ProfesseurDao> findProfesseursByCriteria(Long idEtablissement, Long idCategorie, LocalDate dateDebut, LocalDate dateFin);

}
