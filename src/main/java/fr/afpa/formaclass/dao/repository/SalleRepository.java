package fr.afpa.formaclass.dao.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.formaclass.controller.beans.CriteriaSalle;
import fr.afpa.formaclass.dao.beans.EtablissementDao;
import fr.afpa.formaclass.dao.beans.SalleDao;


public interface SalleRepository extends JpaRepository<SalleDao, Long> {

	@Query("select s from SalleDao s where s.idSalle = ?1 and s.etablissement.idEtablissement = ?2")
	SalleDao findByIdSalleAndEtablissement(Long idSalle, Long idEtablissement);
	@Query("select s from SalleDao s where s.etablissement.idEtablissement = ?1")
	List <SalleDao> findByIdEtablissement(Long idEtablissement);
	
	
	
	//Concerne le service Session
	//@Query("FROM SalleDao s INNER JOIN ReservationDao r ON s.idSalle = r.salle.idSalle where r.dateDebut not between ?3 and ?4 and r.dateFin not between ?3 and ?4 and s.capaciteMax <= ?1 and s.accessibilite = ?2 and s.etablissement.idEtablissement = ?5 ")
	@Query("FROM SalleDao s WHERE s.accessibilite = ?2 and s.capaciteMax >= ?1 and s.etablissement.idEtablissement = ?5 and s.idSalle not in ( select distinct s2.idSalle FROM SalleDao s2 INNER JOIN SessionDao sess2 ON sess2.salle.idSalle = s2.idSalle where sess2.dateDebut between ?3 and ?4 OR sess2.dateFin between ?3 and ?4) and s.idSalle not in  (select distinct s3.idSalle from SalleDao s3 INNER JOIN ReservationDao r3 ON s3.idSalle = r3.salle.idSalle where r3.dateDebut between ?3 and ?4 or r3.dateFin between ?3 and ?4)")
	List<SalleDao> findSalleByCriteria(int capaciteMax, boolean accessibilite, LocalDate debutSession, LocalDate finSession, Long idEtablissement);

		
}
