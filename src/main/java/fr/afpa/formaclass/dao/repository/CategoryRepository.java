package fr.afpa.formaclass.dao.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.afpa.formaclass.dao.beans.CategorieDao;

@Repository
public interface CategoryRepository extends JpaRepository<CategorieDao, Long> {
	
	@Query(" FROM CategorieDao c WHERE c.etablissement.idEtablissement = ?1 or c.etablissement IS NULL")
	List<CategorieDao> findAllByEtablissementAndNull(Long idEtab);
	
	@Query("SELECT c FROM CategorieDao c WHERE (c.idCategorie = ?1 and c.etablissement IS NULL) or (c.idCategorie = ?1 and c.etablissement.idEtablissement = ?2)")
	Optional<CategorieDao> findByidCategorieAndIdEtablissement(Long idCat, Long idEtab);
	
	@Query("Select case when count(c) > 0 then false Else true END FROM CategorieDao c INNER JOIN ModeleDao m ON m.categorie.idCategorie = c.idCategorie where c.idCategorie = ?1 or (c.idCategorie = ?1 and c.etablissement is null)")
	boolean canDeleteOrUpdateCategory(Long idCat);

}
