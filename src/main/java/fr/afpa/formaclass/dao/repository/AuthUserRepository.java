package fr.afpa.formaclass.dao.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.formaclass.dao.beans.AuthUserDao;

public interface AuthUserRepository extends JpaRepository<AuthUserDao, Long> {
	
	Optional<AuthUserDao> findByEmail(String email);
	boolean existsByEmail(String email);
	boolean existsByEmailAndTokenActivationCompte(String email, String token);
	boolean existsByTokenForgetPasswordAndEmail(String token, String email);

}
