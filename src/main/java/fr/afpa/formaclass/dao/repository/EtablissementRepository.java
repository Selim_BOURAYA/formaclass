package fr.afpa.formaclass.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.afpa.formaclass.dao.beans.EtablissementDao;

public interface EtablissementRepository extends JpaRepository<EtablissementDao, Long> {

	
	boolean existsEtablissementDaoByNom(String nom);

	/**
	 * Récupère l'id de l'établissement correspondant grace au mail contenu dans le AuthUser
	 * @param email
	 * @return
	 */
	@Query("select e.idEtablissement FROM EtablissementDao e INNER JOIN AdminDao a ON e.idEtablissement = a.etablissement.idEtablissement INNER JOIN AuthUserDao au on a.authUser.idAuthUser = au.idAuthUser where au.activeProfil = true and au.email = ?1  ")
	Long findIdEtablissementByAuthUserEmailAndActiveProfilTrue(String email);
	
	
	@Query("select e FROM EtablissementDao e INNER JOIN AdminDao a ON e.idEtablissement = a.etablissement.idEtablissement INNER JOIN AuthUserDao au ON a.authUser.idAuthUser = au.idAuthUser where au.email = ?1")
	EtablissementDao findEtablissementByAuthUserMail(String email);
	
	
	
}
