package fr.afpa.formaclass.dao.beans;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class NotificationDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notification_seq_gen")
	@SequenceGenerator(name = "notification_seq_gen", sequenceName = "notification_seq", initialValue = 1, allocationSize = 1 )
	private Long idNotification;
	
	private LocalDate jourAlerte;
	
	private String type;

}
