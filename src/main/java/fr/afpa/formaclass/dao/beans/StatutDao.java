package fr.afpa.formaclass.dao.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import fr.afpa.formaclass.service.beans.EStatut;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class StatutDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "statut_seq_gen")
	@SequenceGenerator(name = "statut_seq_gen", sequenceName = "statut_seq", initialValue = 1, allocationSize = 1 )
	private Long idStatut;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private EStatut nomination;

}

