package fr.afpa.formaclass.dao.beans;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class AdminDao {

	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "admin_seq_gen")
	@SequenceGenerator(name = "admin_seq_gen", sequenceName = "admin_seq", initialValue = 1, allocationSize = 1 )
	private Long idAdmin;
	
	private String nom;
	
	private String prenom;
	
	private String voie;
	
	private String ville;
	
	private String codePostal;
	
	private String telephone;
	
	private boolean actif;
	
	private Date derniereActivite;
	
	private Date dateArchivage;
	
	@NotNull
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idAuthUser", referencedColumnName = "idAuthUser")
	private AuthUserDao authUser;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;
	
}
