package fr.afpa.formaclass.dao.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class SalleDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "salle_seq_gen")
	@SequenceGenerator(name = "salle_seq_gen", sequenceName = "salle_seq", initialValue = 1, allocationSize = 1 )
	private Long idSalle;
	
	private String nom;
	
	private int capaciteMax;
	
	private boolean accessibilite;
	
	private boolean disponible;
	
	private boolean reservable;
		
	@ManyToOne
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;
	
}
