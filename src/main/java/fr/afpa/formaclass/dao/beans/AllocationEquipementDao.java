package fr.afpa.formaclass.dao.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class AllocationEquipementDao {

	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "allocation_seq_gen")
	@SequenceGenerator(name = "allocation_seq_gen", sequenceName = "allocation_seq", initialValue = 1, allocationSize = 1 )
	private Long idAllocation;
	
	private int quantite;
	
	@ManyToOne
	@JoinColumn(name="idEquipement")
	private EquipementDao equipement;
	
	@ManyToOne
	@JoinColumn(name="idSalle", referencedColumnName = "idSalle")
	private SalleDao salle;
		
}
