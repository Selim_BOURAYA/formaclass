package fr.afpa.formaclass.dao.beans;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class AuthUserDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auth_seq_gen")
	@SequenceGenerator(name = "auth_seq_gen", sequenceName = "auth_seq", initialValue = 1, allocationSize = 1 )
	private Long idAuthUser;
	
	@NotNull
	@Column(unique = true)
	private String email;
	
	@NotNull
	private String password;
	
	@ColumnDefault("false")
	private boolean activeProfil;
	
	private String tokenForgetPassword;
	
	private String tokenActivationCompte;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "Role_UserAuth",
			joinColumns = @JoinColumn( name = "idAuthUser"),
			inverseJoinColumns = @JoinColumn(name = "idRole"))
	private Set<RoleDao> roles = new HashSet<>();

}
