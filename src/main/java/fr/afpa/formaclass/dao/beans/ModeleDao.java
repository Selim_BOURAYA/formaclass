package fr.afpa.formaclass.dao.beans;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class ModeleDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "modele_seq_gen")
	@SequenceGenerator(name = "modele_seq_gen", sequenceName = "modele_seq", initialValue = 1, allocationSize = 1 )
	private Long idModele;
	
	private String titre;
	
	private int eleveMin;
	
	private int eleveMax;
	
	private String description;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idCategorie", referencedColumnName = "idCategorie")
	private CategorieDao categorie;
	
//	@ManyToMany (mappedBy = "listeFormationSouhaitees")
//	private Set <EleveDao> eleveInscrits;
	
	@ManyToOne
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;

}
