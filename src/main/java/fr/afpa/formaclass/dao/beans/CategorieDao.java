package fr.afpa.formaclass.dao.beans;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class CategorieDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categorie_seq_gen")
	@SequenceGenerator(name = "categorie_seq_gen", sequenceName = "categorie_seq", initialValue = 1, allocationSize = 1 )
	private Long idCategorie;
	
	private String type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;
	
}
