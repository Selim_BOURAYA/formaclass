package fr.afpa.formaclass.dao.beans;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class EleveDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eleve_seq_gen")
	@SequenceGenerator(name = "eleve_seq_gen", sequenceName = "eleve_seq", initialValue = 1, allocationSize = 1 )
	private Long idEleve;
	
	private String nom;
	
	private String prenom;
	
	private String voie;
	
	private String ville;
	
	private String codePostal;
	
	private String telephone;
	
	@NotNull
	@Column(unique = true)
	private String email;
	
	private boolean actif;
	
	private float note;
	
	private boolean enFormation;
	
	private boolean accepteFormation;
	
	@ManyToMany(fetch = FetchType.LAZY)
	/** suppression (cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)*/
    @JoinTable(
        name = "listeFormationSouhaitees", 
        joinColumns = { @JoinColumn(name = "idEleve") }, 
        inverseJoinColumns = { @JoinColumn(name = "idModele") }
    )
	private Set <ModeleDao> listeFormationSouhaitees;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
        name = "listeSessionEffectuees", 
        joinColumns = { @JoinColumn(name = "idEleve") }, 
        inverseJoinColumns = { @JoinColumn(name = "idSession") }
    )
	private Set <SessionDao> listeSessionEffectuees;
		
	@ManyToOne
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;
}
