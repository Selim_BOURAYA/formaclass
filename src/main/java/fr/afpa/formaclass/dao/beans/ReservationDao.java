package fr.afpa.formaclass.dao.beans;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class ReservationDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservation_seq_gen")
	@SequenceGenerator(name = "reservation_seq_gen", sequenceName = "reservation_seq", initialValue = 1, allocationSize = 1 )
	private Long idReservation;
	
	private String nomination;
	
	private LocalDate dateDebut;
	
	private LocalDate dateFin;
	
	private String information;
	
	@ManyToOne
	@JoinColumn(name="idSalle", referencedColumnName = "idSalle")
	private SalleDao salle;
	
	@ManyToOne
	@JoinColumn(name="idStatut", referencedColumnName = "idStatut")
	private StatutDao statut;
	
	@ManyToOne
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;
}
