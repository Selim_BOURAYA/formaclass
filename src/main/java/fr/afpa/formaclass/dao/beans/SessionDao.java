package fr.afpa.formaclass.dao.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class SessionDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "session_seq_gen")
	@SequenceGenerator(name = "session_seq_gen", sequenceName = "session_seq", initialValue = 1, allocationSize = 1 )
	private Long idSession;
	
	private String nomination;
	
	private LocalDate dateDebut;
	
	private LocalDate dateFin;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="idSessionEnCours")
	private List <EleveDao> listeEleve;
		
	@ManyToOne
	@JoinColumn(name="idModele", referencedColumnName = "idModele")
	private ModeleDao modele;
	
	@ManyToOne
	@JoinColumn(name="idStatut", referencedColumnName = "idStatut")
	private StatutDao statut;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idSalle", referencedColumnName = "idSalle")
	private SalleDao salle;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idProfesseur", referencedColumnName = "idProfesseur")
	private ProfesseurDao professeur;
		
	@OneToOne
	@JoinColumn(name = "idNotification", referencedColumnName = "idNotification")
	private NotificationDao notification;
	
	@ManyToOne
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;

}
