package fr.afpa.formaclass.dao.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class EtablissementDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "etab_seq_gen")
	@SequenceGenerator(name = "etab_seq_gen", sequenceName = "etab_seq", initialValue = 1, allocationSize = 1 )
	private Long idEtablissement;
	
	@Column(unique = true)
	private String nom;
	
	private String voie;
	
	private String ville;
	
	private String email;
	
	private String codePostal;
	
	private String telephone;
	
	@ColumnDefault("false")
	private boolean actif;

	
}
