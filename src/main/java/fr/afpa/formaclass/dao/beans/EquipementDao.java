package fr.afpa.formaclass.dao.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class EquipementDao {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "equipement_seq_gen")
	@SequenceGenerator(name = "equipement_seq_gen", sequenceName = "equipement_seq", initialValue = 1, allocationSize = 1 )
	private Long idEquipement;
	private String nom;
	private boolean disponible;
	private int quantiteTotale;
	private int quantiteAllouee;
		
	@ManyToOne
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;
}