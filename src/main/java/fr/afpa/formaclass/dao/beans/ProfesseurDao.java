package fr.afpa.formaclass.dao.beans;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class ProfesseurDao {

	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prof_seq_gen")
	@SequenceGenerator(name = "prof_seq_gen", sequenceName = "prof_seq", initialValue = 1, allocationSize = 1 )
	private Long idProfesseur;
	
	private String nom;
	
	private String prenom;
	
	private String voie;
	
	private String ville;
	
	private String codePostal;
	
	private String telephone;
	
	@Column(name = "actif", columnDefinition = "boolean default true")
	private boolean actif = true;
	
	private Date derniereActivite;
	
	private Date dateArchivage;
	
	private String specialite;
	
	private boolean enFormation;
	
	
	@ManyToOne
	@JoinColumn(name = "idCategorie", referencedColumnName = "idCategorie")
	private CategorieDao categorie;
	
	@NotNull
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idAuthUser", referencedColumnName = "idAuthUser")
	private AuthUserDao authUser;
	
	@ManyToOne
	@JoinColumn(name = "idEtablissement", referencedColumnName = "idEtablissement")
	private EtablissementDao etablissement;
}
