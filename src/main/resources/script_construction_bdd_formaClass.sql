--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DE L'ETABLISSEMENT ...     ***nuage de fumée***     ... PLEASE CHILL FEW SECONDS ---
--------------------------------------------------------------------------------------------------------

insert into etablissement_dao
values 
(nextval('etab_seq'), 'true','59000','lycee@mail.com','lycee','0344025017','LILLE','42 rue Condorcet');

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES SALLES ...     
--------------------------------------------------------------------------------------------------------
insert into salle_dao
values 
(nextval('salle_seq'),'true',30,'false','salle Marcel','false',1),
(nextval('salle_seq'),'true',25,'false','salle Michel','false',1),
(nextval('salle_seq'),'true',25,'false','salle Martha','false',1),
(nextval('salle_seq'),'true',25,'true','salle Madeleine','true',1);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES EQUIPEMENTS ...     
--------------------------------------------------------------------------------------------------------
insert into equipement_dao
values 
(nextval('equipement_seq'), 'true','Ordinateur',0,30,1),
(nextval('equipement_seq'), 'false','Table',0,60,1),
(nextval('equipement_seq'), 'false','Chaise',0,60,1);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES ROLES ...     
--------------------------------------------------------------------------------------------------------
insert into role_dao
values 
(nextval('role_seq'), 'ROLE_PROFESSEUR'),
(nextval('role_seq'), 'ROLE_ADMIN');

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES COMPTES D'AUTHENTIFICATIONS ...     
--------------------------------------------------------------------------------------------------------
insert into auth_user_dao 
values
(nextval('auth_seq'),'true', 'pierre@mail.com','123','token_activation',null),
(nextval('auth_seq'),'true', 'mohamed@mail.com','123','token_activation',null),
(nextval('auth_seq'),'true', 'danielle@mail.com','123','token_activation',null),
(nextval('auth_seq'),'true', 'amele@mail.com','123','token_activation',null),
(nextval('auth_seq'),'true', 'louis@mail.com','123','token_activation',null);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES ADMINISTRATEURS ...     
--------------------------------------------------------------------------------------------------------
insert into admin_dao 
values
(nextval('admin_seq'),'true','60000', DATE '2021-01-15', DATE '2021-01-24', 'BOU','pierre','0712131415','BEAUVAIS','12 rue Berlioz',1,1),
(nextval('admin_seq'),'true','60110', DATE '2021-01-20', DATE '2021-01-24', 'BOU','mohamed','0712131415','MERU','12 rue Berlioz',2,1);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES CATEGORIES ...     
--------------------------------------------------------------------------------------------------------
insert into categorie_dao 
values
(nextval('categorie_seq'),'informatique'),
(nextval('categorie_seq'),'mathematique'),
(nextval('categorie_seq'),'art'),
(nextval('categorie_seq'),'cuisine');

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES PROFESSEURS ...     
--------------------------------------------------------------------------------------------------------
insert into professeur_dao 
values
(nextval('prof_seq'),'true','80000',DATE '2018-01-15', DATE '2021-01-20','true','SERY','Danielle','cookies,flan,soufflé au fromage','0348966377','AMIENS','11 rue de Lescouvet',3,4,1),
(nextval('prof_seq'),'true','62000',DATE '2019-01-15', DATE '2021-01-10','true','LATYA','Amele','pythagore,fonctions factorielles,intégrales,salsa','0654675423','LILLE','12 rue Salengro',4,1,1),
(nextval('prof_seq'),'true','62000',DATE '2019-01-15', DATE '2021-01-10','true','DUPON','Louis','java,sql,javaScript,judo','0623242526','ARRAS','13 rue des places',5,1,1);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES MODELES ...     
--------------------------------------------------------------------------------------------------------
insert into modele_dao 
values
(nextval('modele_seq'),'conception de gateaux, et de friandises variées',22,15,'concepteur de gateaux',4,1),
(nextval('modele_seq'),'conception de formules mathématiques, et de problèmes parfois épineux',15,12,'concepteur de formules mathématiques',2,1),
(nextval('modele_seq'),'conception de applications (sic), et de machin informatiques compliqués, avec un d apostrophe', 16,14,'concepteur de programmes informatiques du futur',1,1);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES STATUTS ...     
--------------------------------------------------------------------------------------------------------
insert into statut_dao 
values
(nextval('statut_seq'),'en cours'),
(nextval('statut_seq'),'à venir'),
(nextval('statut_seq'),'terminée'),
(nextval('statut_seq'),'annulée');

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES NOTIFICATIONS ...     
--------------------------------------------------------------------------------------------------------
insert into notification_dao
values
(nextval('notification_seq'),DATE '2021-04-15','type 1'),
(nextval('notification_seq'),DATE '2021-06-15','type 2');

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES SESSIONS ...     
--------------------------------------------------------------------------------------------------------
insert into session_dao
values
(nextval('session_seq'),DATE '2021-01-15', DATE '2021-09-20','cuisine_2021_1',1,1,1,1,1,1),
(nextval('session_seq'),DATE '2021-02-15', DATE '2021-10-20','math_2021_1',1,2,1,2,2,2),
(nextval('session_seq'),DATE '2020-01-15', DATE '2020-09-20','cda_2021_1',1,2,1,3,3,3);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES ELEVES ...     
--------------------------------------------------------------------------------------------------------
insert into eleve_dao 
values
(nextval('eleve_seq'),'true','true','69000','martin@mail.fr','true','COMMON',15,'Marty','0612765423','LYON','12 rue des postes',1,1),
(nextval('eleve_seq'),'true','true','59000','annie@mail.fr','true','BRYA',15,'Annie','0612562342','LILLE','22 rue de la république',1,2),
(nextval('eleve_seq'),'true','true','59000','charly@mail.fr','true','BLOND',15,'Charly','0735752412','LYON','1 rue bonheur',1,3);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES ALLOCATIONS MATERIELLES ...     
--------------------------------------------------------------------------------------------------------
insert into allocation_equipement_dao 
values
(nextval('allocation_seq'),5,1,1),
(nextval('allocation_seq'),5,1,2);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES RESERVATIONS ...     
--------------------------------------------------------------------------------------------------------
insert into reservation_dao
values
(nextval('reservation_seq'),DATE '2021-02-15', DATE '2021-02-15','formation premiers secours', 'premiers secours',1,4,2),
(nextval('reservation_seq'),DATE '2021-03-03', DATE '2021-03-05','30 ans de Chantal', 'anniv Chantal',1,4,2);

--------------------------------------------------------------------------------------------------------
 --- INITIALISATION DES FORMATIONS SOUHAITEES / EFFECTUEES ...   A VENIR  
--------------------------------------------------------------------------------------------------------


